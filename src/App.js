import React from "react";
import Header from "./components/Header";
import Footer from "./components/Footer";
import { Container, Content } from "rsuite";
import './assets/style/custom.css';
export default props => {
  return (
    <Container className="dashboard-wrapper">
      <Header />
      <Container>
        <Content>{props.children}</Content>
      </Container>
      <Footer />
    </Container>
  );
};
