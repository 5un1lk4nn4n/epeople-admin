import React from "react";
import { Footer as Foot, FlexboxGrid } from "rsuite";

class Footer extends React.Component {
  render() {
    return (
      <Foot
        style={{
          backgroundColor: "#DCDCDC",
          height: 40
        }}
      >
        <FlexboxGrid
          justify="center"
          style={{
            // backgroundColor: "green",
            height: 40,
            alignItems: "center"
          }}
        >
          <span>
            &copy; 2020{" "}
            <a href="www.goldennest.com" target="_blank">
              Golden Nest Business Private Solutions
            </a>
            . All rights reserved
          </span>
        </FlexboxGrid>
      </Foot>
    );
  }
}

export default Footer;
