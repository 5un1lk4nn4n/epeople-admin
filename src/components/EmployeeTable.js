import React from "react";
import {
  Icon,
  Dropdown,
  Whisper,
  Popover,
  IconButton,
  Table,
  Tag,
  TagGroup
} from "rsuite";

import { ROLES } from "../constants";
const { Column, HeaderCell, Cell, Pagination } = Table;
const COLOR_ROLE = {
  Admin: "blue",
  Sales: "green",
  "Super Admin": "orange"
};

const NameCell = ({ rowData, dataKey, ...props }) => {
  return (
    <Cell {...props}>
      <span>{rowData[dataKey].toLocaleString()}</span>
    </Cell>
  );
};

const StatusCell = ({ rowData, dataKey, ...props }) => {
  return (
    <Cell {...props}>
      <TagGroup>
        <Tag color={rowData[dataKey] ? "green" : "red"}>
          {rowData[dataKey] ? "Active" : "In Active"}
        </Tag>
      </TagGroup>
    </Cell>
  );
};

const RoleCell = ({ rowData, dataKey, ...props }) => {
  return (
    <Cell {...props}>
      <TagGroup>
        <Tag color={COLOR_ROLE[rowData[dataKey]]}>
          {rowData[dataKey].toLocaleString()}
        </Tag>
      </TagGroup>
    </Cell>
  );
};

const Menu = ({ onSelect, isActive }) => (
  <Dropdown.Menu onSelect={onSelect}>
    <Dropdown.Item eventKey={3}>
      {isActive ? "Make Inactive" : "Make Active"}
    </Dropdown.Item>
  </Dropdown.Menu>
);

const MenuPopover = ({ onSelect, isActive, isRoot, ...rest }) => (
  <Popover {...rest} full>
    {isRoot ? (
      <span>No action available</span>
    ) : (
      <Menu onSelect={onSelect} isActive={isActive} />
    )}
  </Popover>
);

let tableBody;

class CustomWhisper extends React.Component {
  constructor(props) {
    super(props);
    this.handleSelectMenu = this.handleSelectMenu.bind(this);
  }
  handleSelectMenu(eventKey, event) {
    const { ban, data } = this.props;
    this.trigger.hide();
    ban(data.id, !data.is_active);
  }
  render() {
    const { data } = this.props;
    console.log(data, "df");
    return (
      <Whisper
        placement="autoVerticalStart"
        trigger="click"
        triggerRef={ref => {
          this.trigger = ref;
        }}
        container={() => {
          return tableBody;
        }}
        speaker={
          <MenuPopover
            onSelect={this.handleSelectMenu}
            isActive={data.is_active}
            isRoot={data.role === "Super Admin"}
          />
        }
      >
        {this.props.children}
      </Whisper>
    );
  }
}

const ActionCell = ({ rowData, dataKey, ban, ...props }) => {
  return (
    <Cell {...props} className="link-group">
      <CustomWhisper data={rowData} ban={ban}>
        <IconButton appearance="subtle" icon={<Icon icon="more" />} />
      </CustomWhisper>
    </Cell>
  );
};

class EmployeeTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      checkedKeys: [],
      data: this.props.data,
      displayLength: 10,
      loading: false,
      page: 1
    };
    this.handleChangePage = this.handleChangePage.bind(this);
    this.handleChangeLength = this.handleChangeLength.bind(this);
  }

  handleChangePage(dataKey) {
    this.props.onPageChange(dataKey);
  }
  handleChangeLength(dataKey) {}

  render() {
    const {
      toggleActive,
      data,
      loading,
      maxPage,
      activePage,
      role
    } = this.props;

    return (
      <div>
        <Pagination
          lengthMenu={[
            {
              value: 10,
              label: 10
            }
          ]}
          activePage={activePage}
          displayLength={10}
          total={maxPage}
          pages={maxPage}
          ellipsis
          onChangePage={this.handleChangePage}
          onChangeLength={this.handleChangeLength}
          boundaryLinks
        />
        <Table
          height={600}
          data={data}
          loading={loading}
          id="table"
          bodyRef={ref => {
            tableBody = ref;
          }}
        >
          <Column width={300} align="center">
            <HeaderCell>Role</HeaderCell>
            <RoleCell dataKey="role" />
          </Column>

          <Column width={300}>
            <HeaderCell>Name</HeaderCell>
            <NameCell dataKey="name" />
          </Column>

          <Column width={200}>
            <HeaderCell>Mobile</HeaderCell>
            <NameCell dataKey="mobile" />
          </Column>

          <Column width={150} align="center">
            <HeaderCell>Status</HeaderCell>
            <StatusCell dataKey="is_active" />
          </Column>
          {role === ROLES.SUPER_ADMIN ? (
            <Column width={200}>
              <HeaderCell>Action</HeaderCell>
              <ActionCell dataKey="id" ban={toggleActive} />
            </Column>
          ) : null}
        </Table>
      </div>
    );
  }
}

export default EmployeeTable;
