const MENU = [
  {
    name: 'Dashboard',
    url: '/dashboard',
    icon: 'home',
    color: 'red',
  },
  {
    name: 'Bill Board',
    url: '/billboard',
    icon: 'square',
    color: '#7b241c',
  },
  {
    name: 'Offers',
    url: '/offers',
    icon: 'gift',
    color: '#2E8B57',
  },

  {
    name: 'Inventory',
    url: '/inventories',
    icon: 'server',
    color: '#7d3c98',
  },
  {
    name: 'Employees',
    url: '/employees',
    icon: 'users',
    color: '#cb4335',
  },
  {
    name: 'Settings',
    url: '/setting',
    icon: 'slider',
    color: 'blue',

    submenu: [
      {
        name: 'Profile',
        url: '/settings/profile',
        color: '#d4ac0d',
      },
    ],
  },
  {
    name: 'Qbee Account',
    url: '/qbee_account',
    icon: 'umbrella',
    color: '#239b56',
    submenu: [
      {
        name: 'Dashboard',
        url: '/qbee/dashboard',
        color: 'orange',
      },
      {
        name: 'Settings',
        url: '/qbee/settings',
        color: '#a4ad0d',
      },
    ],
  },
];

export default MENU;
