import React, { Component, useState, useEffect } from "react";

import ImageUploader from "react-images-upload";
import Chips from "./../components/Chips";

import {
  Icon,
  Alert,
  Radio,
  RadioGroup,
  Steps,
  Container,
  FlexboxGrid,
  Form,
  FormControl,
  FormGroup,
  HelpBlock,
  Input,
  InputGroup,
  AutoComplete,
  Toggle,
  ControlLabel,
  Button,
  ButtonGroup,
  Divider,
  SelectPicker
} from "rsuite";
import { geolocated } from "react-geolocated";

// api
import ApiService from "./../services/api";

import Loader from "./Loader";
var validator = require("validator");
var QRCode = require("qrcode.react");

const state = ["Tamilnadu"];

const VendorInfo = ({
  setFormValues,
  vendor,
  listCategories,
  role,
  vendorInfo,
  isEdit,
  pictures,
  onPictureUplod,
  onFileUploadSuccess,
  searchCat,
  ...props
}) => {
  const getHashTags = () => {
    if (isEdit) {
      let hashTags = vendorInfo.hash_tag;
      if (hashTags) {
        return hashTags.split("#");
      } else {
        return [];
      }
    } else {
      return [];
    }
  };

  const [hasCoupon, setHasCoupon] = useState(false);

  useEffect(() => {
    setHasCoupon(vendorInfo.has_coupon);
  }, [vendorInfo.has_coupon]);

  return (
    <>
      {["3", "5"].indexOf(role) > -1 ? (
        <FormGroup className="large">
          <ControlLabel>Employee Code</ControlLabel>

          <InputGroup>
            <InputGroup.Addon>EPM</InputGroup.Addon>
            <Input
              type="number"
              onChange={value => setFormValues("sales_by_code", value)}
              value={vendorInfo.sales_by_code}
            />
          </InputGroup>
          <HelpBlock>Enter only number part of Employee Code</HelpBlock>
        </FormGroup>
      ) : null}
      <FormGroup>
        <ControlLabel>Company Name</ControlLabel>
        <FormControl
          name="company"
          componentClass="input"
          value={vendorInfo.company_name}
          onChange={value => setFormValues("company_name", value)}
        />
        <HelpBlock>
          <span className="mandatory">*</span>Mandatory
        </HelpBlock>
      </FormGroup>
      <FormGroup>
        <ControlLabel>Address line 1</ControlLabel>
        <FormControl
          name="address"
          componentClass="input"
          value={vendorInfo.address_line_1}
          onChange={value => setFormValues("address_line_1", value)}
        />
        <HelpBlock>
          <span className="mandatory">*</span>Mandatory
        </HelpBlock>
      </FormGroup>
      <FormGroup>
        <ControlLabel>Address line 2</ControlLabel>
        <FormControl
          name="address"
          componentClass="input"
          value={vendorInfo.address_line_2}
          onChange={value => setFormValues("address_line_2", value)}
        />
        <HelpBlock>
          <span className="mandatory">*</span>Mandatory
        </HelpBlock>
      </FormGroup>
      <FormGroup>
        <ControlLabel>District</ControlLabel>
        <FormControl
          name="district"
          value="Coimbatore"
          onChange={value => setFormValues("district", value)}
        />
        <HelpBlock>
          <span className="mandatory">*</span>Mandatory
        </HelpBlock>
      </FormGroup>
      <FormGroup>
        <ControlLabel>Pin Code</ControlLabel>
        <FormControl
          name="pincode"
          value={vendorInfo.pin_code}
          onChange={value => setFormValues("pin_code", value)}
        />
        <HelpBlock>
          <span className="mandatory">*</span>Mandatory
        </HelpBlock>
      </FormGroup>
      <FormGroup>
        <ControlLabel>State</ControlLabel>
        <InputGroup inside>
          <AutoComplete
            data={state}
            value="Tamilnadu"
            onChange={value => setFormValues("state", value)}
          />
          <InputGroup.Button>
            <Icon icon="search" />
          </InputGroup.Button>
          <HelpBlock>
            <span className="mandatory">*</span>Mandatory
          </HelpBlock>
        </InputGroup>
      </FormGroup>

      <FormGroup>
        <ControlLabel>Company Phone Number</ControlLabel>
        <FormControl
          name="phone"
          componentClass="input"
          value={vendorInfo.company_phone}
          onChange={value => setFormValues("company_phone", value)}
        />
      </FormGroup>
      <FormGroup>
        <ControlLabel>Owner Name</ControlLabel>
        <FormControl
          name="name"
          componentClass="input"
          value={vendorInfo.owner_name}
          onChange={value => setFormValues("owner_name", value)}
        />
        <HelpBlock>
          <span className="mandatory">*</span>Mandatory
        </HelpBlock>
      </FormGroup>
      <FormGroup>
        <ControlLabel>Owner Mobile</ControlLabel>
        <FormControl
          name="mobile"
          componentClass="input"
          value={vendorInfo.mobile}
          onChange={value => setFormValues("mobile", value)}
        />
        <HelpBlock>
          <span className="mandatory">*</span>Mandatory.This will be used as
          username for login.
        </HelpBlock>
      </FormGroup>
      <FormGroup>
        <ControlLabel>Email</ControlLabel>
        <FormControl
          name="email"
          type="email"
          componentClass="input"
          value={vendorInfo.email}
          onChange={value => setFormValues("email", value)}
        />
      </FormGroup>
      <FormGroup>
        <ControlLabel>Website</ControlLabel>
        <FormControl
          name="website"
          componentClass="input"
          value={vendorInfo.website}
          onChange={value => setFormValues("website", value)}
        />
      </FormGroup>
      <FormGroup>
        <ControlLabel>GST Number</ControlLabel>
        <FormControl
          name="gst"
          componentClass="input"
          value={vendorInfo.gst}
          onChange={value => setFormValues("gst", value)}
        />
      </FormGroup>
      <FormGroup>
        <ControlLabel>Business Category</ControlLabel>
        <SelectPicker
          data={listCategories}
          value={vendorInfo.category}
          onChange={value => setFormValues("category", value)}
          onSearch={value => searchCat(value)}
        />
        <HelpBlock>
          <span className="mandatory">*</span>Mandatory
        </HelpBlock>
      </FormGroup>
      <FormGroup className="large">
        <ControlLabel>Vendor Hashes </ControlLabel>
        <Chips
          renderSearchTerm={value => setFormValues("hash", value)}
          hashTags={getHashTags()}
        />
        <HelpBlock>
          Hash tags for the vendor. Eg: non veg, organic or any.
        </HelpBlock>
      </FormGroup>
      <FormGroup controlId="radioList" className="large">
        <ControlLabel>Business type</ControlLabel>
        <RadioGroup
          name="radioList"
          inline
          value={vendorInfo.business_type}
          onChange={value => setFormValues("business_type", value)}
        >
          <Radio value="SM">Small</Radio>
          <Radio value="MD">Medium</Radio>
          <Radio value="LR">Large</Radio>
        </RadioGroup>
        <HelpBlock>
          <span className="mandatory">*</span>Mandatory
        </HelpBlock>
      </FormGroup>
      <FormGroup className="large">
        <ControlLabel>Discount Percentage</ControlLabel>
        <InputGroup>
          <Input
            onChange={value => setFormValues("discount1", value)}
            value={vendorInfo.discount1}
          />
          <InputGroup.Addon>%</InputGroup.Addon>

          <InputGroup.Addon>-</InputGroup.Addon>
          <Input
            onChange={value => setFormValues("discount2", value)}
            value={vendorInfo.discount2}
          />
          <InputGroup.Addon>%</InputGroup.Addon>
        </InputGroup>
        <HelpBlock>
          <span className="mandatory">*</span>Mandatory
        </HelpBlock>
      </FormGroup>
      <FormGroup className="large">
        <ControlLabel>Discount Coupon</ControlLabel>

        <Toggle
          size="lg"
          checkedChildren="Yes"
          unCheckedChildren="No"
          checked={hasCoupon}
          onChange={value => {
            setFormValues("has_coupon", value);
            setHasCoupon(!hasCoupon);
          }}
        />
      </FormGroup>
      {hasCoupon ? (
        <>
          <FormGroup className="large">
            <ControlLabel>Coupon Value</ControlLabel>

            <InputGroup>
              <InputGroup.Addon>₹</InputGroup.Addon>
              <Input
                onChange={value => setFormValues("coupon_value", value)}
                value={vendorInfo.coupon_value}
              />
              <InputGroup.Addon>.00</InputGroup.Addon>

              <InputGroup.Addon>Count</InputGroup.Addon>
              <Input
                onChange={value => setFormValues("coupon_count", value)}
                value={vendorInfo.coupon_count}
              />
            </InputGroup>
          </FormGroup>
          <FormGroup className="large">
            <ControlLabel>Coupon Description</ControlLabel>

            <Input
              componentClass="textarea"
              rows={3}
              value={vendorInfo.coupon_description}
              style={{ width: 300, resize: "auto" }}
              onChange={value => setFormValues("coupon_description", value)}
            />
          </FormGroup>
        </>
      ) : null}

      {isEdit ? (
        <FormGroup>
          <ControlLabel>Agreement upload</ControlLabel>

          <ImageUploader
            withPreview
            withIcon={true}
            buttonText="Choose images"
            onChange={pictures => {
              onPictureUplod(pictures);
              onFileUploadSuccess();
            }}
            imgExtension={[".jpg", ".gif", ".png", ".gif", "jpeg"]}
            maxFileSize={5242880}
            singleImage
          />
        </FormGroup>
      ) : null}
    </>
  );
};

const UploapAgreement = ({
  pictures,
  onPictureUplod,
  onFileUploadSuccess,
  onSelect,
  activeKey,
  logout,
  role,
  ...props
}) => {
  if (!props.newVendor.qr_code) {
    return <Loader />;
  }
  return (
    <>
      <FormGroup>
        <ControlLabel>Company QR Code</ControlLabel>
        <FormControl
          name=""
          componentClass="input"
          disabled
          value={props.newVendor.qr_code}
        />
      </FormGroup>
      <FormGroup>
        <ControlLabel>QR Code</ControlLabel>
        <QRCode value={props.newVendor.qr_code} />
      </FormGroup>
      <FormGroup>
        <ImageUploader
          withPreview
          withIcon={true}
          buttonText="Choose images"
          onChange={pictures => {
            onPictureUplod(pictures);
            onFileUploadSuccess();
          }}
          imgExtension={[".jpg", ".gif", ".png", ".gif", "jpeg"]}
          maxFileSize={5242880}
          singleImage
        />
      </FormGroup>
    </>
  );
};

class VendorRegistration extends Component {
  vendorForm = {
    hasCoupon: false
  };
  api = new ApiService();
  constructor(props) {
    super(props);
    this.state = {
      loader: true,
      step: 0,
      progressLoader: false,
      disabled: false,
      isFileuploaded: false,
      latitude: null,
      longitude: null,
      hasCoupon: false,
      editLoading: false
    };
    this.decline = this.decline.bind(this);
    this.increase = this.increase.bind(this);
  }

  componentDidMount() {
    this.vendorForm["location"] = this.props.coords;
    const { isVendorEdit } = this.props;
    if (isVendorEdit) {
      this.loadVendorInfo();
    }
  }
  loadVendorInfo = () => {
    const { vendorInfo } = this.props;
    if (!vendorInfo) {
      return;
    }

    this.setState({
      company_name: vendorInfo.company_name,
      address_line_1: vendorInfo.location.address_line_1,
      address_line_2: vendorInfo.location.address_line_2,
      district: vendorInfo.location.district,
      pin_code: vendorInfo.location.pin_code,
      state: vendorInfo.location.state,
      company_phone: vendorInfo.company_phone,
      owner_name: vendorInfo.owner_name,
      mobile: vendorInfo.mobile,
      email: vendorInfo.email,
      website: vendorInfo.website,
      gst: vendorInfo.gst,
      category: vendorInfo.category.id,
      hash_tag: vendorInfo.hash_tag,
      business_type: vendorInfo.business_type,
      discount1: vendorInfo.discount_percentage.split("-")[0],
      discount2: vendorInfo.discount_percentage.split("-")[1],
      has_coupon: vendorInfo.has_coupon,
      coupon_value: vendorInfo.coupon_value,
      coupon_count: vendorInfo.coupon_count,
      coupon_description: vendorInfo.coupon_description,
      longitude: vendorInfo.location.longitude,
      latitude: vendorInfo.location.latitude,
      sales_by_code: vendorInfo.sales_by_code
    });
  };
  changePercent(nextstep) {
    const step = nextstep < 0 ? 0 : nextstep > 1 ? 1 : nextstep;
    this.setState({
      step
    });
  }
  decline() {
    this.changePercent(this.state.step - 1);
  }
  getRandomLatLngGen(from, to, fixed) {
    return (Math.random() * (to - from) + from).toFixed(fixed) * 1;
  }
  increase() {
    var numberFormat = /^\d{10}$/;
    const { step, isFileuploaded } = this.state;
    if (step === 0) {
      this.setState({
        progressLoader: false
      });

      if (
        !this.vendorForm.owner_name ||
        !this.vendorForm.company_name ||
        !this.vendorForm.mobile ||
        !this.vendorForm.address_line_1 ||
        !this.vendorForm.address_line_2 ||
        // !this.vendorForm.state ||
        // !this.vendorForm.district ||
        !this.vendorForm.pin_code ||
        !this.vendorForm.category ||
        !this.vendorForm.business_type ||
        !this.vendorForm.discount1 ||
        !this.vendorForm.discount2
      ) {
        Alert.error("All mandatory fields are required", 5000);
        return;
      }

      if (
        this.vendorForm.has_coupon &&
        (!this.vendorForm.coupon_value || !this.vendorForm.coupon_count)
      ) {
        Alert.error("Coupon is choosen but no coupon values provided", 5000);
        return;
      }

      if (
        (this.vendorForm.mobile && !(this.vendorForm.mobile.length == 10)) ||
        !this.vendorForm.mobile.match(numberFormat)
      ) {
        Alert.error("Mobile Number is not valid");
        return;
      }

      if (this.vendorForm.email && !validator.isEmail(this.vendorForm.email)) {
        Alert.error("Email is not valid");
        return;
      }

      // NOTE: VALIDATION FOR DICOUNT is PENDING
      if (
        !(parseFloat(this.state.discount1) >= 0) ||
        !(parseFloat(this.state.discount2) <= 100)
      ) {
        Alert.error("Discount Percentage is not valid");
        return;
      }

      if (
        parseFloat(this.state.discount1) !== 0 &&
        parseFloat(this.state.discount2) !== 0 &&
        parseFloat(this.state.discount1) > parseFloat(this.state.discount2)
      ) {
        Alert.error("Discount Percentage is not valid");
        return;
      }

      let latitude, longitude;

      if (["3", "5"].indexOf(this.props.role) > -1) {
        if (!this.state.latitude || !this.state.longitude) {
          Alert.error("Location is Mandatory", 5000);
          return;
        }
        latitude = this.state.latitude;
        longitude = this.state.longitude;
      } else if (this.props.coords) {
        latitude = this.props.coords.latitude;
        longitude = this.props.coords.longitude;
      } else {
        Alert.error(
          "Location information not found. Set on location service and try again"
        );
        return;
      }
      if (this.props.role != 4) {
        if (
          !(this.vendorForm.sales_by_code > 0) ||
          !(this.vendorForm.sales_by_code < 1000)
        ) {
          Alert.error("Employee code should be between 0 and 1000");
          return;
        }
      }
      var vendorFirstForm = {
        company_name: this.vendorForm.company_name,
        location: {
          latitude,
          longitude,
          address_line_1: this.vendorForm.address_line_1,
          address_line_2: this.vendorForm.address_line_2,
          city: this.vendorForm.city,
          pin_code: this.vendorForm.pin_code,
          district: "Coimbatore",
          state: "Tamilnadu"
        },
        owner_name: this.vendorForm.owner_name,
        mobile: this.vendorForm.mobile,
        category: this.vendorForm.category,
        website: this.vendorForm.website,
        email: this.vendorForm.email,
        gst: this.vendorForm.gst,
        business_type: this.vendorForm.business_type,
        discount_percentage:
          this.vendorForm.discount1 + " - " + this.vendorForm.discount2,
        has_coupon: this.vendorForm.has_coupon,
        hash_tag: this.vendorForm.hash,
        coupon_value: this.vendorForm.coupon_value,
        coupon_count: this.vendorForm.coupon_count,
        company_phone: this.vendorForm.company_phone,
        coupon_description: this.vendorForm.coupon_description,
        sales_by_code: this.vendorForm.sales_by_code,
        step: 1
      };
      this.props.newVendorCreation(vendorFirstForm, () => {
        this.changePercent(this.state.step + 1);
        this.setState({
          progressLoader: false
        });
      });
    } else if (step === 1) {
      if (!isFileuploaded) {
        Alert.error("Please upload agreement soft copy", 5000);
        return;
      }
      this.setState({
        progressLoader: true
      });
      this.props.vendorSubmission(responseCode => {
        var isComplete = false;
        if (responseCode === 200) {
          isComplete = true;
        }
        this.setState({
          progressLoader: false,
          disabled: isComplete
        });
      });
    }
  }

  renderStepForm = () => {
    const { step } = this.state;
    const {
      listCategories,
      pictures,
      newVendor,
      role,
      isVendorEdit
    } = this.props;
    console.log(this.state, "rendering");
    switch (step) {
      case 0:
      default:
        return (
          <VendorInfo
            setFormValues={this.setFormValues}
            vendor={this.props.vendorData}
            listCategories={listCategories}
            searchCat={this.props.searchCat}
            role={role}
            vendorInfo={this.state}
            isEdit={isVendorEdit}
            onPictureUplod={this.props.onPictureUplod}
            onFileUploadSuccess={() => {
              // alert("hi");
              this.setState({
                isFileuploaded: true
              });
            }}
            pictures={pictures}
          />
        );

      case 1:
        return (
          <UploapAgreement
            onPictureUplod={this.props.onPictureUplod}
            onFileUploadSuccess={() => {
              // alert("hi");
              this.setState({
                isFileuploaded: true
              });
            }}
            pictures={pictures}
            newVendor={newVendor}
          />
        );
    }
  };

  setFormValues = (name, value) => {
    console.log(name, value);

    this.vendorForm[name] = value;

    this.setState({ [name]: value });

    // console.log(this.vendorForm);
  };

  updateVendor = () => {
    const { editVendorInfo, vendorInfo } = this.props;
    var numberFormat = /^\d{10}$/;
    if (
      !this.state.owner_name ||
      !this.state.company_name ||
      !this.state.mobile ||
      !this.state.address_line_1 ||
      !this.state.address_line_2 ||
      // !this.state.state ||
      // !this.state.district ||
      !this.state.pin_code ||
      !this.state.category ||
      !this.state.business_type ||
      !this.state.discount1 ||
      !this.state.discount2
    ) {
      Alert.error("All mandatory fields are required", 5000);
      return;
    }

    if (
      this.state.has_coupon &&
      (!this.state.coupon_value || !this.state.coupon_count)
    ) {
      Alert.error("Coupon is choosen but no coupon values provided", 5000);
      return;
    }

    if (
      (this.state.mobile && !(this.state.mobile.length == 10)) ||
      !this.state.mobile.match(numberFormat)
    ) {
      Alert.error("Mobile Number is not valid");
      return;
    }

    if (this.state.email && !validator.isEmail(this.state.email)) {
      Alert.error("Email is not valid");
      return;
    }

    // NOTE: VALIDATION FOR DICOUNT is PENDING
    if (
      !(parseFloat(this.state.discount1) >= 0) ||
      !(parseFloat(this.state.discount2) <= 100)
    ) {
      Alert.error("Discount Percentage is not valid");
      return;
    }

    if (
      parseFloat(this.state.discount1) !== 0 &&
      parseFloat(this.state.discount2) !== 0 &&
      parseFloat(this.state.discount1) > parseFloat(this.state.discount2)
    ) {
      Alert.error("Discount Percentage is not valid");
      return;
    }
    let latitude, longitude;

    if (["3", "5"].indexOf(this.props.role) > -1) {
      if (!this.state.latitude || !this.state.longitude) {
        Alert.error("Location is Mandatory", 5000);
        return;
      }
    } else {
      Alert.error(
        "Location information not found. Set on location service and try again"
      );
      return;
    }
    if (this.props.role != 4) {
      if (
        !(this.state.sales_by_code > 0) ||
        !(this.state.sales_by_code < 1000)
      ) {
        Alert.error("Employee code should be between 0 and 1000");
        return;
      }
    }
    var vendorFirstForm = {
      company_name: this.state.company_name,
      location: {
        id: vendorInfo.location.id,
        latitude: this.state.latitude,
        longitude: this.state.longitude,
        address_line_1: this.state.address_line_1,
        address_line_2: this.state.address_line_2,
        city: this.state.city,
        pin_code: this.state.pin_code,
        district: "Coimbatore",
        state: "Tamilnadu"
      },
      owner_name: this.state.owner_name,
      mobile: this.state.mobile,
      category: this.state.category,
      website: this.state.website,
      email: this.state.email,
      gst: this.state.gst,
      business_type: this.state.business_type,
      discount_percentage: this.state.discount1 + " - " + this.state.discount2,
      has_coupon: this.state.has_coupon,
      hash_tag: this.state.hash,
      coupon_value: this.state.has_coupon ? this.state.coupon_value : null,
      coupon_count: this.state.has_coupon ? this.state.coupon_count : null,
      company_phone: this.state.company_phone,
      coupon_description: this.state.has_coupon
        ? this.state.coupon_description
        : null,
      sales_by_code: this.state.sales_by_code
    };
    const newValues = {
      vendor_id: vendorInfo.id,
      update_type: 4,
      new_values: vendorFirstForm
    };
    this.setState({
      editLoading: true
    });
    editVendorInfo(newValues, code => {
      this.setState({
        editLoading: false
      });
    });
  };

  render() {
    const { step, progressLoader, disabled } = this.state;
    const { vendorInfo, role, isVendorEdit } = this.props;
    if (!this.props.isGeolocationAvailable) {
      Alert.error("Geolocation is not available.", 5000);
    }
    if (!this.props.isGeolocationEnabled) {
      Alert.error(
        "Geolocation is not enabled. Please turn on and try again",
        5000
      );
    }

    return (
      <Container>
        <FlexboxGrid
          style={{
            flexDirection: "column"
          }}
          className="dashboard-forms"
        >
          <div
            style={{
              width: "100%"
            }}
          >
            {!isVendorEdit ? (
              <Steps current={step}>
                <Steps.Item title="Vendor Information" />
                <Steps.Item title="Upload Agreement" />
              </Steps>
            ) : null}
          </div>
          <Divider />

          {["3", "5"].indexOf(role) > -1 ? (
            <InputGroup>
              <InputGroup.Addon>LAT</InputGroup.Addon>
              <Input
                onChange={value => {
                  this.setState({
                    latitude: value
                  });
                }}
                value={this.state.latitude}
              />

              <InputGroup.Addon>LONG</InputGroup.Addon>
              <Input
                onChange={value => {
                  this.setState({
                    longitude: value
                  });
                }}
                value={this.state.longitude}
              />
            </InputGroup>
          ) : this.props.coords ? (
            <div>
              <strong>Location: </strong>
              {` ${this.props.coords.latitude}|${this.props.coords.longitude}`}
            </div>
          ) : (
            <div>Location not available</div>
          )}

          <Divider />
          <Form>
            {this.renderStepForm()}
            {!isVendorEdit ? (
              <FormGroup className="large">
                <Divider />
                <ButtonGroup>
                  <Button
                    onClick={this.decline}
                    disabled={step === 0}
                    appearance="primary"
                    disabled={disabled}
                  >
                    Previous
                  </Button>
                  <Button
                    onClick={this.increase}
                    appearance="primary"
                    loading={progressLoader}
                    disabled={disabled}
                  >
                    {step === 0 ? "Next" : "Finish"}
                  </Button>
                </ButtonGroup>
              </FormGroup>
            ) : (
              <FormGroup className="large">
                <Divider />
                <ButtonGroup>
                  <Button
                    onClick={this.updateVendor}
                    appearance="primary"
                    loading={this.state.editLoading}
                  >
                    Update Vendor
                  </Button>
                </ButtonGroup>
              </FormGroup>
            )}
          </Form>
        </FlexboxGrid>
      </Container>
    );
  }
}

export default geolocated({
  positionOptions: {
    enableHighAccuracy: true
  },
  userDecisionTimeout: 5000
})(VendorRegistration);
