import React, { Component } from "react";
import {
  Sidenav,
  Icon,
  Nav,
  Container,
  FlexboxGrid,
  Avatar,
  Form,
  FormGroup,
  FormControl,
  ControlLabel,
  HelpBlock,
  ButtonToolbar,
  IconButton,
  Alert
} from "rsuite";
import Loader from "../components/Loader";
import ApiService from "../services/api";
// constants
const styles = {
  width: 250,
  display: "inline-table",
  marginRight: 10,
  height: "100%"
};

const ROLE = ["VENDOR", "USER", "ADMIN", "SALES", "SUPER ADMIN"];

const SidenavInstance = ({ setSection, ...props }) => {
  const { isSubnavActivated } = props;
  return (
    <div
      style={styles}
      className={
        isSubnavActivated
          ? "custom-navigation-drawyer active"
          : "custom-navigation-drawyer"
      }
    >
      <Icon
        icon="bars"
        className="custom-navigation-drawyer__activator"
        onClick={() => props.activateSideNav()}
      />
      <Sidenav
        {...props}
        defaultOpenKeys={["3", "4"]}
        style={{
          height: "100%"
        }}
      >
        <Sidenav.Body>
          <Nav>
            <Nav.Item
              active
              eventKey="1"
              icon={<Icon icon="user" />}
              onClick={() => {
                setSection(1);
              }}
            >
              Your Profile
            </Nav.Item>
            <Nav.Item
              eventKey="2"
              icon={<Icon icon="key" />}
              onClick={() => {
                setSection(2);
              }}
            >
              Change Password
            </Nav.Item>
          </Nav>
        </Sidenav.Body>
      </Sidenav>
    </div>
  );
};

class Settings extends Component {
  api = new ApiService();
  constructor(props) {
    super(props);
    this.state = {
      show: false,
      activeSection: 1,
      loader: true,
      passwordLoader: false,
      isSubnavActivated: false,
      msg:
        "It should contain mixed number, smaller and bigger alphabets or special characters. Minimum length 8"
    };
  }

  componentDidMount() {
    this.api
      .myProfile()
      .then(res => {
        if (res.code === 401) {
          Alert.error(res.message, 5000);
        }
        if (res.code === 400) {
          Alert.error(res.message, 5000);
        }
        if (res.code === 200) {
          this.setState({
            userInfo: res.data,
            loader: false
          });
        }
      })
      .catch(error => {
        this.setState({
          loading: false
        });
        console.log("error");
        console.log(error);
      });
  }

  onChangePassword = (name, value) => {
    var mediumRegex = new RegExp(
      "^(?=.{7,})(((?=.*[A-Z])(?=.*[a-z]))|((?=.*[A-Z])(?=.*[0-9]))|((?=.*[a-z])(?=.*[0-9]))).*$",
      "g"
    );
    if (name === "newPassword") {
      if (false === mediumRegex.test(value)) {
        this.setState({
          msg:
            "Your new password is weak!. It should contain mixed number, smaller and bigger alphabets or special character"
        });
      } else {
        this.setState({
          msg: "Your new password is strong!"
        });
      }
    }

    this.setState({ [name]: value });
  };

  confirmChangePassword = () => {
    const { oldPassword, newPassword, newPasswordR } = this.state;
    if (!oldPassword || !newPassword || !newPasswordR) {
      Alert.error("All fields are mandatory!");
      return;
    }
    if (newPassword !== newPasswordR) {
      Alert.error("New password Fields are not same!");
      return;
    }
    if (newPassword.length < 8) {
      Alert.error("New password should be mininum of 8 characters!");
      return;
    }
    this.setState({
      passwordLoader: true,
      isDisabled: true
    });
    this.api
      .changePassword(oldPassword, newPassword)
      .then(res => {
        if (res.code === 401) {
          Alert.error(res.message, 5000);
          this.setState({
            isDisabled: false,
            passwordLoader: false
          });
        }
        if (res.code === 400) {
          Alert.error(res.message, 5000);
          this.setState({
            isDisabled: false,
            passwordLoader: false
          });
        }
        if (res.code === 200) {
          Alert.success("Password changed");

          this.setState({
            passwordLoader: false,
            isDisabled: true
          });
        }
      })
      .catch(error => {
        this.setState({
          loading: false
        });
        console.log("error");
        console.log(error);
      });
  };

  renderContainer = () => {
    const {
      activeSection,
      loader,
      userInfo,
      passwordLoader,
      isDisabled,
      msg
    } = this.state;
    if (activeSection === 1) {
      if (loader) {
        return <Loader />;
      }
      return (
        <content>
          <Avatar
            circle
            size="lg"
            style={{
              marginBottom: "2%"
            }}
          >
            {userInfo.name.charAt(0)}
          </Avatar>
          <Form>
            <FormGroup>
              <ControlLabel>Name</ControlLabel>
              <FormControl name="name" value={userInfo.name} disabled />
            </FormGroup>
            <FormGroup>
              <ControlLabel>Mobile Number</ControlLabel>
              <FormControl
                name="mobile"
                type="text"
                value={userInfo.mobile}
                disabled
              />
              <HelpBlock tooltip>Your login username</HelpBlock>
            </FormGroup>
            <FormGroup>
              <ControlLabel>Designation</ControlLabel>
              <FormControl
                name="role"
                type="text"
                value={ROLE[parseInt(userInfo.role - 1)]}
                disabled
              />
            </FormGroup>
          </Form>
        </content>
      );
    }
    if (activeSection === 2) {
      return (
        <content>
          <Form>
            <FormGroup>
              <ControlLabel>Old Password</ControlLabel>
              <FormControl
                name="name"
                type="password"
                onChange={value => {
                  this.onChangePassword("oldPassword", value);
                }}
              />
            </FormGroup>
            <FormGroup>
              <ControlLabel>New Password</ControlLabel>
              <FormControl
                type="password"
                onChange={value => {
                  this.onChangePassword("newPassword", value);
                }}
              />
              <HelpBlock>{msg}</HelpBlock>
            </FormGroup>
            <FormGroup>
              <ControlLabel>New Password again</ControlLabel>
              <FormControl
                type="password"
                onChange={value => {
                  this.onChangePassword("newPasswordR", value);
                }}
              />
            </FormGroup>

            <FormGroup>
              <ButtonToolbar>
                <IconButton
                  loading={passwordLoader}
                  disabled={isDisabled}
                  appearance="primary"
                  icon={<Icon icon={"arrow-right"} />}
                  onClick={() => this.confirmChangePassword()}
                >
                  Change
                </IconButton>
                {/* <Button appearance="default">Cancel</Button> */}
              </ButtonToolbar>
            </FormGroup>
          </Form>
        </content>
      );
    }
  };

  setActiveSection = section => {
    this.setState({
      activeSection: section
    });
    this.activateSideNav();
  };

  activateSideNav = () => {
    this.setState({ isSubnavActivated: !this.state.isSubnavActivated });
  };
  render() {
    return (
      <FlexboxGrid
        style={{
          flexDirection: "row",
          height: window.innerHeight
        }}
      >
        <SidenavInstance
          setSection={this.setActiveSection}
          appearance="subtle"
          isSubnavActivated={this.state.isSubnavActivated}
          activateSideNav={this.activateSideNav}
        />
        <Container className="settings-container">
          {this.renderContainer()}
        </Container>
      </FlexboxGrid>
    );
  }
}

export default Settings;
