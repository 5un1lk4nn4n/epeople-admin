import React, { Component } from "react";

import { Container, FlexboxGrid } from "rsuite";

import { MEDIA_URL } from "../constants/urls";

const APPROVAL_STATUS = {
  P: "Pending",
  A: "Approved",
  D: "Denied"
};
class OfferInfo extends Component {
  vendorForm = {};
  constructor(props) {
    super(props);
    this.state = {
      loader: true,
      step: 0
    };
  }

  componentDidMount() {}

  render() {
    const { data } = this.props;
    return (
      <Container className="vendorInformation-wrapper">
        <FlexboxGrid
          style={{
            flexDirection: "column"
          }}
          className="vendorInformation-wrapper__grid"
        >
          <table className="vendorInformation-wrapper__table">
            <tbody>
              <tr>
                <img
                  src={`${MEDIA_URL}${data["banner"]}`}
                  width="160"
                  alt="offer banner"
                />
              </tr>
              <tr>
                <td>Offer Title</td>
                <td>:</td>
                <td>{data["title"]}</td>
              </tr>
              <tr>
                <td>Offer Description</td>
                <td>:</td>
                <td>{data["description"]}</td>
              </tr>
              <tr>
                <td>Discount</td>
                <td>:</td>
                <td>{data["discount"]}</td>
              </tr>
              <tr>
                <td>From Date</td>
                <td>:</td>
                <td>{data["from_date"]}</td>
              </tr>
              <tr>
                <td>To Date</td>
                <td>:</td>
                <td>{data["to_date"]}</td>
              </tr>
              <tr>
                <td>Approval Status</td>
                <td>:</td>
                <td>{APPROVAL_STATUS[data["approval_status"]]}</td>
              </tr>
              <tr>
                <td>Is Active?</td>
                <td>:</td>
                <td>{data["is_active"] ? "Yes" : "No"}</td>
              </tr>

              <tr>
                <td>Vendor</td>
                <td>:</td>
                <td>{data["vendor_shop_name"]}</td>
              </tr>
              <tr>
                <td>Vendor Contact Mobile</td>
                <td>:</td>
                <td>{data["mobile"]}</td>
              </tr>
            </tbody>
          </table>
        </FlexboxGrid>
      </Container>
    );
  }
}

export default OfferInfo;
