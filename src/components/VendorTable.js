import React from "react";
import {
  Icon,
  Dropdown,
  Whisper,
  Popover,
  IconButton,
  Divider,
  Table,
  Tag,
  TagGroup,
  Tooltip,
  Alert
} from "rsuite";

import { CopyToClipboard } from "react-copy-to-clipboard";
var QRCode = require("qrcode.react");

const { Column, HeaderCell, Cell, Pagination } = Table;

const tooltip = <Tooltip>View</Tooltip>;
const NameCell = ({ rowData, dataKey, ...props }) => {
  return (
    <Cell {...props}>
      <a>{rowData[dataKey].toLocaleString()}</a>
    </Cell>
  );
};

const ShopTypeCell = ({ rowData, dataKey, ...props }) => {
  return (
    <Cell {...props}>
      <span>{rowData[dataKey].name}</span>
      {/* <Icon icon={rowData[dataKey].icon} size="lg" /> */}
    </Cell>
  );
};

const RegStatusCell = ({ rowData, dataKey, ...props }) => {
  return (
    <Cell {...props}>
      <TagGroup>
        <Tag color={rowData[dataKey] ? "green" : "red"}>
          {rowData[dataKey] ? "Completed" : "Incomplete"}
        </Tag>
      </TagGroup>
    </Cell>
  );
};

const StatusCell = ({ rowData, dataKey, ...props }) => {
  return (
    <Cell {...props}>
      <TagGroup>
        <Tag color={rowData[dataKey] ? "green" : "red"}>
          {rowData[dataKey] ? "Active" : "Inactive"}
        </Tag>
      </TagGroup>
    </Cell>
  );
};

const ImageCell = ({ rowData, dataKey, ...props }) => (
  <Cell {...props} style={{ padding: 0 }}>
    <div
      style={{
        width: 40,
        height: 40,
        background: "#f5f5f5",
        marginTop: 2,
        overflow: "hidden",
        display: "inline-block"
      }}
    >
      {/* <img src= width="44" /> */}
      <QRCode value={rowData[dataKey]} size={40} />
    </div>
  </Cell>
);

const Menu = ({ onSelect, role, isActive, qrCode, isRegComplete }) => {
  console.log(role, qrCode, isActive, [3, 5].indexOf(role));
  return (
    <Dropdown.Menu onSelect={onSelect}>
      {["3", "5"].indexOf(role) > -1 && isRegComplete ? (
        <Dropdown.Item eventKey={1}>
          {isActive ? "Make Inactive" : "Make Active"}
        </Dropdown.Item>
      ) : null}
      {["3", "5"].indexOf(role) > -1 ? (
        <Dropdown.Item eventKey={2}>Edit</Dropdown.Item>
      ) : null}
      <Dropdown.Item eventKey={3}>
        <CopyToClipboard
          text={qrCode}
          onCopy={() => Alert.error(`QR code ${qrCode} copied`)}
        >
          <span>Copy QR Code</span>
        </CopyToClipboard>
      </Dropdown.Item>
    </Dropdown.Menu>
  );
};

const MenuPopover = ({
  onSelect,
  role,
  isActive,
  qrCode,
  isRegComplete,
  ...rest
}) => (
  <Popover {...rest} full>
    <Menu
      onSelect={onSelect}
      role={role}
      isActive={isActive}
      qrCode={qrCode}
      isRegComplete={isRegComplete}
    />
  </Popover>
);

let tableBody;

class CustomWhisper extends React.Component {
  constructor(props) {
    super(props);
    this.handleSelectMenu = this.handleSelectMenu.bind(this);
  }
  handleSelectMenu(eventKey, event) {
    const { onViewClick, dataId, updateVendor, editVendor } = this.props;
    switch (eventKey) {
      case 1:
        const newValues = {
          vendor_id: dataId,
          update_type: 1
        };
        updateVendor(newValues, () => {});
        break;
      case 2:
        editVendor(dataId);
        break;
      case 3:
        break;
      default:
        break;
    }
    this.trigger.hide();
  }
  render() {
    const { role, isActive, qrCode, isRegComplete } = this.props;
    // console.log(qrCode);
    return (
      <Whisper
        placement="autoVerticalStart"
        trigger="click"
        triggerRef={ref => {
          this.trigger = ref;
        }}
        container={() => {
          return tableBody;
        }}
        speaker={
          <MenuPopover
            onSelect={this.handleSelectMenu}
            role={role}
            isActive={isActive}
            isRegComplete={isRegComplete}
            qrCode={qrCode}
          />
        }
      >
        {this.props.children}
      </Whisper>
    );
  }
}

const ActionCell = ({
  rowData,
  dataKey,
  onViewClick,
  ban,
  edit,
  updateVendor,
  role,
  editVendor,
  ...props
}) => {
  // console.log(rowData["qr_code"]);
  return (
    <Cell {...props} className="link-group">
      <Whisper placement="bottom" trigger="hover" speaker={tooltip}>
        <IconButton
          onClick={() => {
            onViewClick(rowData[dataKey]);
          }}
          appearance="subtle"
          icon={<Icon icon="eye" />}
        />
      </Whisper>
      <Divider vertical />
      <CustomWhisper
        onViewClick={onViewClick}
        role={role}
        dataId={rowData[dataKey]}
        isActive={rowData["is_active"]}
        isRegComplete={rowData["is_registration_complete"]}
        qrCode={rowData["qr_code"]}
        updateVendor={updateVendor}
        editVendor={editVendor}
      >
        <IconButton appearance="subtle" icon={<Icon icon="more" />} />
      </CustomWhisper>
    </Cell>
  );
};

class VendorTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      checkedKeys: [],
      displayLength: 10,
      loading: false,
      page: 1
    };
  }

  handleChangePage = dataKey => {
    this.props.onPageChange(dataKey);
  };
  handleChangeLength = dataKey => {
    this.setState({
      page: 1,
      displayLength: dataKey
    });
  };

  render() {
    const {
      loading,
      data,
      maxPage,
      onViewClick,
      activePage,
      role
    } = this.props;
    // console.log(maxPage);
    const { ban, edit, updateVendor, editVendor } = this.props;

    return (
      <div>
        <Pagination
          lengthMenu={[
            {
              value: 10,
              label: 10
            }
          ]}
          activePage={activePage}
          displayLength={10}
          pages={maxPage}
          ellipsis
          boundaryLinks
          total={maxPage}
          onChangePage={this.handleChangePage}
          onChangeLength={this.handleChangeLength}
        />
        <Table
          height={600}
          data={data}
          loading={loading}
          id="table"
          bodyRef={ref => {
            tableBody = ref;
          }}
        >
          <Column width={150} align="center">
            <HeaderCell>QR Code</HeaderCell>
            <ImageCell dataKey="qr_code" />
          </Column>
          <Column width={160}>
            <HeaderCell>Code</HeaderCell>
            <NameCell dataKey="qr_code" />
          </Column>
          <Column width={160}>
            <HeaderCell>Company Name</HeaderCell>
            <NameCell dataKey="company_name" />
          </Column>

          <Column width={160}>
            <HeaderCell>Owner Name</HeaderCell>
            <NameCell dataKey="owner_name" />
          </Column>

          <Column width={150}>
            <HeaderCell>Mobile</HeaderCell>
            <NameCell dataKey="mobile" />
          </Column>
          <Column width={100} align="center">
            <HeaderCell>Status</HeaderCell>
            <StatusCell dataKey="is_active" />
          </Column>
          <Column width={100} align="center">
            <HeaderCell>Registration Status</HeaderCell>
            <RegStatusCell dataKey="is_registration_complete" />
          </Column>
          <Column width={120} align="center">
            <HeaderCell>Category</HeaderCell>
            <ShopTypeCell dataKey="category" />
          </Column>

          <Column width={200}>
            <HeaderCell>Action</HeaderCell>
            <ActionCell
              dataKey="id"
              ban={ban}
              edit={edit}
              updateVendor={updateVendor}
              onViewClick={onViewClick}
              role={role}
              editVendor={editVendor}
            />
          </Column>
        </Table>
      </div>
    );
  }
}

export default VendorTable;
