import React from "react";
import { Link, Redirect } from "react-router-dom";
import { Icon, Navbar, Header as Head, Modal, Button, Avatar } from "rsuite";
import Nav from "@rsuite/responsive-nav";
import { HEADER_MENU } from "../constants";

const NavBarInstance = ({ onSelect, activeKey, logout, role, ...props }) => {
  const menu = HEADER_MENU[role];

  return (
    <Head>
      <Navbar {...props} className="dashboard-header">
        <Navbar.Header
          style={{
            // backgroundColor: "#030e4e",
            paddingRight: "2%",
            display: "flex",
            alignItems: "center",
            justifyContent: "center"
          }}
        >
          {/* <img src={logo} height={40} /> */}
          <Avatar
            circle
            style={{ backgroundColor: "#004B93", color: "#fff", marginLeft: 5 }}
          >
            EP
          </Avatar>
          <strong
            style={{
              marginLeft: 2
            }}
          >
            Epeoplemart
          </strong>
        </Navbar.Header>
        <Navbar.Body>
          <Nav onSelect={onSelect}>
            {menu.map((item, key) => {
              return (
                <Nav.Item
                  eventKey={item.eventKey}
                  icon={<Icon icon={item.icon} />}
                  to={item.url}
                  componentClass={Link}
                  key={item.name}
                >
                  {item.name}
                </Nav.Item>
              );
            })}
          </Nav>
          <Nav pullRight>
            <Nav.Item
              eventKey="7"
              icon={<Icon icon="cog" />}
              to="/settings"
              componentClass={Link}
            >
              Settings
            </Nav.Item>

            <Nav.Item
              icon={<Icon icon="power-off" />}
              eventKey="8"
              onClick={() => {
                logout();
              }}
            ></Nav.Item>
          </Nav>
        </Navbar.Body>
      </Navbar>
    </Head>
  );
};

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.handleSelect = this.handleSelect.bind(this);
    this.state = {
      activeKey: null,
      show: false
    };
    this.close = this.close.bind(this);
    this.open = this.open.bind(this);
  }

  close() {
    this.setState({
      show: false
    });
  }
  open(size) {
    this.setState({
      size,
      show: true
    });
  }

  handleSelect(eventKey) {
    this.setState({
      activeKey: eventKey
    });
  }

  getRole() {
    return localStorage.getItem("rl");
  }

  render() {
    const { activeKey, isLogout } = this.state;
    const role = this.getRole();

    if (isLogout) {
      return <Redirect to="/login" push={true} />;
    }
    return (
      <div className="nav-wrapper">
        <NavBarInstance
          activeKey={activeKey}
          onSelect={this.handleSelect}
          appearance="subtle"
          role={role}
          logout={() => {
            this.open("md");
          }}
        />
        <Modal
          size="sm"
          show={this.state.show}
          onHide={this.close}
          class="custom-dialog"
        >
          <Modal.Header>
            <Modal.Title>Confirm Logout?</Modal.Title>
          </Modal.Header>
          <Modal.Footer>
            <Button
              color="red"
              onClick={() => {
                localStorage.clear();
                this.setState({
                  isLogout: true
                });
              }}
            >
              <Icon icon="sign-out" /> log out
            </Button>
            <Button onClick={this.close} appearance="subtle">
              Cancel
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

export default Header;
