import React, { Component } from "react";
import Chips from "./../components/Chips";
import ImageUploader from "react-images-upload";
import {
  Icon,
  Alert,
  Content,
  Container,
  FlexboxGrid,
  Form,
  FormControl,
  FormGroup,
  HelpBlock,
  ButtonToolbar,
  ControlLabel,
  Button,
  Input,
  InputGroup,
  IconButton,
  ButtonGroup,
  Modal,
  Drawer,
  DatePicker,
  SelectPicker
} from "rsuite";
// constants
import Table from "../components/OfferTable";
import Loader from "../components/Loader";
import ApiService from "../services/api";
import OfferInfo from "../components/OfferInfo";

const FILTER_BUTTON = [
  {
    name: "All",
    key: "all"
  },
  {
    name: "Active",
    key: "active"
  },
  {
    name: "In Active",
    key: "inactive"
  },
  {
    name: "Pending",
    key: "pending"
  },
  {
    name: "Approved",
    key: "approved"
  }
];

class Offers extends Component {
  api = new ApiService();
  publishOfferReq = {};
  constructor(props) {
    super(props);
    this.state = {
      show: false,
      showDrawer: false,
      currentFilter: "all",
      pageLoader: false,
      tableLoader: false
    };
    this.close = this.close.bind(this);
    this.open = this.open.bind(this);
    this.closeDrawer = this.closeDrawer.bind(this);
    this.toggleDrawer = this.toggleDrawer.bind(this);
  }

  componentDidMount() {
    this.role = localStorage.getItem("rl");
    this.getOffer();
  }
  close() {
    this.setState({
      show: false
    });
  }
  open(size) {
    this.setState({
      size,
      show: true
    });
  }

  onSearch = filter => {
    this.search = filter;
    this.setState({
      tableLoader: true
    });
    this.getOffer(this.getFilter());
  };

  getFilter = () => {
    let filter = "";
    if (this.page) {
      filter = `page=${this.page}`;
    }
    if (this.search) {
      filter = `${filter}&search=${this.search}`;
    }
    return filter;
  };

  // list vendors
  vendorList = () => {
    const listVendorsReq = {
      apiURL: "vendors"
    };
    this.api.getAPIRequest(listVendorsReq).then(res => {
      res.data.forEach(el => {
        return (el.label = el.company_name), (el.value = el.id);
      });
      this.setState({ vendorsList: res.data });
    });
  };

  selectedVendor = (name, value) => {
    this.publishOfferReq[name] = value;
  };

  getOffer = filter => {
    this.vendorList();
    this.api
      .getOffer(filter)
      .then(res => {
        this.setState({
          pageLoader: false,
          tableLoader: false
        });
        if (res.code === 401) {
          Alert.error(res.message, 5000);
        }
        if (res.code === 400) {
          Alert.error(res.message, 5000);
        }
        if (res.code === 200) {
          this.maxPage = res.max_pages;
          this.setState({
            OfferList: res.data
          });
        }
      })
      .catch(error => {
        this.setState({
          loading: false
        });
        console.log("error");
      });
  };

  closeDrawer() {
    this.setState(
      {
        showDrawer: false
      },
      () => {
        this.offerInfo = null;
        this.isView = false;
      }
    );
  }
  toggleDrawer(placement) {
    this.setState({
      placement,
      showDrawer: true
    });
  }
  filterOffer = label => {
    let fil = null;
    switch (label) {
      default:
      case "active":
      case "inactive":
        fil = `is_active=${label === "active" ? 1 : 0}`;
        break;
      case "pending":
      case "approved":
        fil = `approval_status=${label === "pending" ? "P" : "A"}`;
        break;
    }
    this.setState({
      currentFilter: label
    });

    this.getOffer(fil);
  };

  onPageChange = page => {
    this.page = page;

    this.getOffer(this.getFilter());
  };

  onViewClick = offerInfo => {
    this.offerInfo = offerInfo;
    this.isView = true;
    this.setState(
      {
        drawerLoader: true
      },
      () => {
        // this.getVendorDetails();
      }
    );
    this.toggleDrawer("right");
  };

  publishOffer = (name, value) => {
    this.publishOfferReq[name] = value;
    // this.toggleDrawer("right");
  };

  publishOfferHandler = () => {
    const request = {
      apiURL: "offers",
      request: this.publishOfferReq
    };
    this.api.postAPIRequest(request).then(res => {
      if (res.code === 400 || res.code === 401) {
        Alert.error(res.message, 5000);
      } else if (res.code === 200) {
        Alert.success(res.message, 5000);
        this.closeDrawer();
        // this.getOffer()
      }
    });
  };

  renderSearchTerm = searchTerm => {
    this.publishOfferReq["tag"] = searchTerm;
  };

  // format date
  formatDate = (name, selectedDate) => {
    var date = new Date(selectedDate);
    let y = date.getFullYear();
    let m = date.getMonth() + 1;
    let d = date.getDate();
    if (m.length < 2) m = "0" + m;
    if (d.length < 2) d = "0" + d;
    this.publishOfferReq[name] = [y, m, d].join("-");
  };

  // image upload
  onPictureUplod = picture => {
    const fd = new FormData();
    fd.append("file", picture[0], picture[0].name);
    fd.append("file_type", "A");
    this.api.uploadImg(fd).then(res => {
      if (res.code === 400) {
        Alert.error(res.message, 5000);
      } else if (res.code === 200 || res.code === 201) {
        Alert.success(res.message, 5000);
        this.setState({ imgUploadData: res.data });
        this.publishOfferReq["banner"] = res.data.id;
      }
    });
  };

  updateOffer = newOfferValues => {
    this.api.updateOffer(newOfferValues).then(res => {
      this.getOffer(this.getFilter());

      if (res.code === 400) {
        Alert.error(res.message, 5000);
      } else if (res.code === 200 || res.code === 201) {
        Alert.success(res.message, 5000);
      }
    });
  };

  render() {
    const { currentFilter, pageLoader, OfferList, vendorsList } = this.state;
    if (pageLoader) {
      return <Loader />;
    }

    return (
      <Content>
        <div
          style={{
            marginTop: "2%"
          }}
        >
          <Container>
            <FlexboxGrid justify="center" className="offers-captions">
              <FlexboxGrid.Item
                style={{
                  marginRight: "5%"
                }}
              >
                <InputGroup size="sm">
                  <Input
                    placeholder="Search by Shop Name or Tags"
                    onChange={this.onSearch}
                  />
                  <InputGroup.Addon appearance="primary">
                    <Icon icon="search" />
                  </InputGroup.Addon>
                </InputGroup>
              </FlexboxGrid.Item>
              <FlexboxGrid.Item>
                <ButtonGroup size="sm">
                  {FILTER_BUTTON.map((label, key) => {
                    return (
                      <Button
                        onClick={() => this.filterOffer(label.key)}
                        key={`${label}-btn-${key}`}
                        appearance={
                          currentFilter === label.key ? "primary" : "default"
                        }
                      >
                        {label.name}
                      </Button>
                    );
                  })}
                </ButtonGroup>
              </FlexboxGrid.Item>
              <ButtonToolbar>
                <IconButton
                  appearance="primary"
                  icon={<Icon icon={"plus"} />}
                  size="sm"
                  onClick={() => this.toggleDrawer("right")}
                >
                  New Offer
                </IconButton>
              </ButtonToolbar>
            </FlexboxGrid>
          </Container>
        </div>

        <Table
          data={OfferList}
          maxPage={this.maxPage}
          onPageChange={this.onPageChange}
          onViewClick={this.onViewClick}
          updateOffer={this.updateOffer}
        />
        <Drawer
          placement={this.state.placement}
          show={this.state.showDrawer}
          onHide={this.closeDrawer}
          className="custom-drawyer"
          backdrop="static"
        >
          <Drawer.Header>
            <Drawer.Title>
              {this.isView ? "View Offer" : "New Offer"}
            </Drawer.Title>
          </Drawer.Header>
          <Drawer.Body>
            {this.isView ? (
              <OfferInfo data={this.offerInfo} />
            ) : (
              <Container>
                <FlexboxGrid
                  style={{
                    flexDirection: "column"
                  }}
                >
                  <Form className="dashboard-forms">
                    <FormGroup className="large">
                      <ControlLabel>Offer Title</ControlLabel>
                      <FormControl
                        name="title"
                        componentClass="input"
                        onChange={value => this.publishOffer("title", value)}
                      />
                    </FormGroup>
                    <FormGroup className="large">
                      <ControlLabel>Offer Description</ControlLabel>
                      <FormControl
                        rows={3}
                        name="description"
                        componentClass="textarea"
                        onChange={value =>
                          this.publishOffer("description", value)
                        }
                      />
                    </FormGroup>
                    <FormGroup>
                      <ControlLabel>Offer discount</ControlLabel>
                      <FormControl
                        name="title"
                        componentClass="input"
                        onChange={value => this.publishOffer("discount", value)}
                      />
                    </FormGroup>
                    <div className="clearfix"></div>
                    <FormGroup>
                      <ControlLabel>From </ControlLabel>
                      <DatePicker
                        format="YYYY-MM-DD"
                        onChange={value => this.formatDate("from_date", value)}
                      />
                      <HelpBlock tooltip>Required</HelpBlock>
                    </FormGroup>
                    <FormGroup>
                      <ControlLabel>Expired On</ControlLabel>
                      <DatePicker
                        format="YYYY-MM-DD"
                        onChange={value => this.formatDate("to_date", value)}
                      />
                      <HelpBlock tooltip>Required</HelpBlock>
                    </FormGroup>
                    <FormGroup className="large">
                      <ControlLabel>Offer Search Terms</ControlLabel>
                      <Chips
                        renderSearchTerm={this.renderSearchTerm}
                        hashTags={[]}
                      />
                      <HelpBlock>Hash tags for the Offer</HelpBlock>
                    </FormGroup>
                    {this.role !== "1" ? (
                      <FormGroup>
                        <ControlLabel>Select Vendor</ControlLabel>
                        <SelectPicker
                          data={vendorsList}
                          onChange={value =>
                            this.selectedVendor("vendor_id", value)
                          }
                          onSearch={search => {
                            this.api.getVendor(`search=${search}`).then(res => {
                              res.data.forEach(el => {
                                return (
                                  (el.label = el.company_name),
                                  (el.value = el.id)
                                );
                              });
                              this.setState({ vendorsList: res.data });
                            });
                          }}
                        />
                      </FormGroup>
                    ) : null}

                    <FormGroup className="large">
                      <ControlLabel>Poster</ControlLabel>
                      <ImageUploader
                        withIcon={true}
                        buttonText="Choose images"
                        onChange={this.onPictureUplod}
                        imgExtension={[".jpg", ".gif", ".png", ".gif", "jpeg"]}
                        maxFileSize={5242880}
                      />
                      <HelpBlock tooltip>
                        Image should be in standard format
                      </HelpBlock>
                    </FormGroup>
                    <FormGroup>
                      <ButtonToolbar>
                        <Button
                          color="blue"
                          onClick={() => this.publishOfferHandler()}
                        >
                          <Icon icon="check-circle" /> Publish Offer
                        </Button>
                        <Button appearance="subtle" onClick={this.closeDrawer}>
                          Cancel
                        </Button>
                      </ButtonToolbar>
                    </FormGroup>
                  </Form>
                </FlexboxGrid>
              </Container>
            )}
          </Drawer.Body>
        </Drawer>
        <Modal size="sm" show={this.state.show} onHide={this.close}>
          <Modal.Header>
            <Modal.Title>Deactivate Offer?</Modal.Title>
          </Modal.Header>
          {/* <Modal.Body>
            <Paragraph />
          </Modal.Body> */}
          <Modal.Footer>
            <IconButton
              icon={<Icon icon="check-circle" />}
              onClick={this.close}
            >
              Yes
            </IconButton>
            <Button onClick={this.close} appearance="subtle">
              Cancel
            </Button>
          </Modal.Footer>
        </Modal>
      </Content>
    );
  }
}

export default Offers;
