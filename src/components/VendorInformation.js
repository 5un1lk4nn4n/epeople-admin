import React, { Component } from "react";

import { Container, FlexboxGrid } from "rsuite";

var QRCode = require("qrcode.react");

class VendorInformation extends Component {
  vendorForm = {};
  constructor(props) {
    super(props);
    this.state = {
      loader: true,
      step: 0
    };
  }

  componentDidMount() {}

  render() {
    const { data } = this.props;
    return (
      <Container className="vendorInformation-wrapper">
        <FlexboxGrid
          style={{
            flexDirection: "column"
          }}
          className="vendorInformation-wrapper__grid"
        >
          <QRCode value={data["qr_code"]} size={100} />
          <table className="vendorInformation-wrapper__table">
            <tbody>
              <tr>
                <td>QR Code</td>
                <td>:</td>
                <td>{data["qr_code"]}</td>
              </tr>
              <tr>
                <td>Company Name</td>
                <td>:</td>
                <td>{data["company_name"]}</td>
              </tr>
              <tr>
                <td>Phone</td>
                <td>:</td>
                <td>{data["company_phone"]}</td>
              </tr>
              <tr>
                <td>Website</td>
                <td>:</td>
                <td>{data["website"]}</td>
              </tr>
              <tr>
                <td>Email</td>
                <td>:</td>
                <td>{data["email"]}</td>
              </tr>
              <tr>
                <td>Address</td>
                <td>:</td>
                <td>
                  {data["location"]["address_line_1"] +
                    ", " +
                    data["location"]["address_line_2"] +
                    ", " +
                    data["location"]["pin_code"] +
                    "," +
                    data["location"]["district"] +
                    ", " +
                    data["location"]["state"]}
                </td>
              </tr>
              <tr>
                <td>Owner Name</td>
                <td>:</td>
                <td>{data["owner_name"]}</td>
              </tr>
              <tr>
                <td>Mobile</td>
                <td>:</td>
                <td>{data["mobile"]}</td>
              </tr>
              <tr>
                <td>gst</td>
                <td>:</td>
                <td>{data["gst"]}</td>
              </tr>
              <tr>
                <td>Category</td>
                <td>:</td>
                <td>{data["category"]["name"]}</td>
              </tr>
              <tr>
                <td>Business Type</td>
                <td>:</td>
                <td>
                  {data["business_type"].toLowerCase() === "sm"
                    ? "Small"
                    : data["business_type"].toLowerCase() === "md"
                    ? "Medium"
                    : data["business_type"].toLowerCase() === "lr"
                    ? "Large"
                    : ""}
                </td>
              </tr>
              <tr>
                <td>Discount %</td>
                <td>:</td>
                <td>{data["discount_percentage"]}</td>
              </tr>
              <tr>
                <td>Have Coupon?</td>
                <td>:</td>
                <td>{data["has_coupon"] ? "Yes" : "No"}</td>
              </tr>
              {data["has_coupon"] ? (
                <tr>
                  <td>Coupon Value</td>
                  <td>:</td>
                  <td>{data["coupon_value"]}</td>
                </tr>
              ) : null}

              {data["has_coupon"] ? (
                <tr>
                  <td>Coupon Count</td>
                  <td>:</td>
                  <td>{data["coupon_count"]}</td>
                </tr>
              ) : null}
              {data["has_coupon"] ? (
                <tr>
                  <td>Coupon Description</td>
                  <td>:</td>
                  <td>{data["coupon_description"]}</td>
                </tr>
              ) : null}
            </tbody>
          </table>
        </FlexboxGrid>
      </Container>
    );
  }
}

export default VendorInformation;
