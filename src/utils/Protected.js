/* eslint-disable no-unused-vars */
import React from "react";
import { Route, Redirect } from "react-router-dom";
import ApiService from "../services/api";
import App from "../App";

const Protected = ({ component: Component, ...rest }) => {
  const auth = new ApiService();
  const token = auth.getToken();

  // const token = "1";

  if (token) {
    // auth.tokenVerify(token).then(res => {});
    return (
      <Route
        render={props => (
          <App>
            <Component {...rest} />
          </App>
        )}
      />
    );
  }
  return (
    <Redirect
      to={{ pathname: "/login", state: { from: rest.location.pathname } }}
    />
  );
};

export default Protected;
