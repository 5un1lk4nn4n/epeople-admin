import React from "react";
import {
  Icon,
  Dropdown,
  Whisper,
  Popover,
  IconButton,
  Divider,
  Table,
  Tag,
  TagGroup,
  Tooltip
} from "rsuite";

var QRCode = require("qrcode.react");
const { Column, HeaderCell, Cell, Pagination } = Table;

const tooltip = <Tooltip>View</Tooltip>;
const NameCell = ({ rowData, dataKey, ...props }) => {
  return (
    <Cell {...props}>
      <a>{rowData[dataKey].toLocaleString()}</a>
    </Cell>
  );
};

const ShopTypeCell = ({ rowData, dataKey, ...props }) => {
  return (
    <Cell {...props}>
      <Icon icon={rowData[dataKey].icon} size="lg" />
    </Cell>
  );
};

const StatusCell = ({ rowData, dataKey, ...props }) => {
  return (
    <Cell {...props}>
      <TagGroup>
        <Tag
          color={
            rowData[dataKey].toLocaleString() === "inactive" ? "red" : "green"
          }
        >
          {rowData[dataKey].toLocaleString()}
        </Tag>
      </TagGroup>
    </Cell>
  );
};

const ImageCell = ({ rowData, dataKey, ...props }) => (
  <Cell {...props} style={{ padding: 0 }}>
    <div
      style={{
        width: 40,
        height: 40,
        background: "#f5f5f5",
        marginTop: 2,
        overflow: "hidden",
        display: "inline-block"
      }}
    >
      {/* <img src= width="44" /> */}
      <QRCode value={rowData[dataKey]} size={40} />
    </div>
  </Cell>
);

const Menu = ({ onSelect }) => (
  <Dropdown.Menu onSelect={onSelect}>
    <Dropdown.Item eventKey={1}>Edit</Dropdown.Item>
    <Dropdown.Item eventKey={2}>Copy QR Code</Dropdown.Item>
  </Dropdown.Menu>
);

const MenuPopover = ({ onSelect, ...rest }) => (
  <Popover {...rest} full>
    <Menu onSelect={onSelect} />
  </Popover>
);

let tableBody;

class CustomWhisper extends React.Component {
  constructor(props) {
    super(props);
    this.handleSelectMenu = this.handleSelectMenu.bind(this);
  }
  handleSelectMenu(eventKey, event) {
    const { onViewClick, dataId } = this.props;
    switch (eventKey) {
      case 1:
      default:
        onViewClick(dataId);
        break;
    }
    this.trigger.hide();
  }
  render() {
    return (
      <Whisper
        placement="autoVerticalStart"
        trigger="click"
        triggerRef={ref => {
          this.trigger = ref;
        }}
        container={() => {
          return tableBody;
        }}
        speaker={<MenuPopover onSelect={this.handleSelectMenu} />}
      >
        {this.props.children}
      </Whisper>
    );
  }
}

const ActionCell = ({ rowData, dataKey, onViewClick, ban, edit, ...props }) => {
  return (
    <Cell {...props} className="link-group">
      <Whisper placement="bottom" trigger="hover" speaker={tooltip}>
        <IconButton
          onClick={() => {
            onViewClick(rowData[dataKey]);
          }}
          appearance="subtle"
          icon={<Icon icon="eye" />}
        />
      </Whisper>
      <Divider vertical />
      <CustomWhisper onViewClick={onViewClick} dataId={rowData[dataKey]}>
        <IconButton appearance="subtle" icon={<Icon icon="more" />} />
      </CustomWhisper>
    </Cell>
  );
};

class VendorTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      checkedKeys: [],
      displayLength: 10,
      loading: false,
      page: 1
    };
  }

  handleChangePage = dataKey => {
    this.props.onPageChange(dataKey);
  };
  handleChangeLength = dataKey => {
    this.setState({
      page: 1,
      displayLength: dataKey
    });
  };

  render() {
    const { page } = this.state;
    const { loading, data, maxPage, onViewClick } = this.props;
    const { ban, edit } = this.props;

    return (
      <div>
        <Pagination
          lengthMenu={[
            {
              value: 10,
              label: 10
            }
          ]}
          activePage={page}
          displayLength={10}
          total={maxPage}
          onChangePage={this.handleChangePage}
          onChangeLength={this.handleChangeLength}
        />
        <Table
          height={600}
          data={data}
          loading={loading}
          id="table"
          bodyRef={ref => {
            tableBody = ref;
          }}
        >
          <Column width={150} align="center">
            <HeaderCell>QR Code</HeaderCell>
            <ImageCell dataKey="qr_code" />
          </Column>
          <Column width={160}>
            <HeaderCell>Code</HeaderCell>
            <NameCell dataKey="qr_code" />
          </Column>
          <Column width={160}>
            <HeaderCell>Company Name</HeaderCell>
            <NameCell dataKey="company_name" />
          </Column>

          <Column width={160}>
            <HeaderCell>Owner Name</HeaderCell>
            <NameCell dataKey="owner_name" />
          </Column>

          <Column width={240}>
            <HeaderCell>Mobile</HeaderCell>
            <NameCell dataKey="mobile" />
          </Column>
          <Column width={100} align="center">
            <HeaderCell>Status</HeaderCell>
            <StatusCell dataKey="is_active" />
          </Column>
          <Column width={80} align="center">
            <HeaderCell>Category</HeaderCell>
            <ShopTypeCell dataKey="category" />
          </Column>

          <Column width={200}>
            <HeaderCell>Action</HeaderCell>
            <ActionCell
              dataKey="id"
              ban={ban}
              edit={edit}
              onViewClick={onViewClick}
            />
          </Column>
        </Table>
      </div>
    );
  }
}

export default VendorTable;
