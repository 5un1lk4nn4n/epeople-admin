import React from "react";
import {
  Icon,
  Dropdown,
  Whisper,
  Checkbox,
  Popover,
  IconButton,
  Divider,
  Table,
  Tag,
  TagGroup,
  Tooltip
} from "rsuite";
import { MEDIA_URL } from "../constants/urls";
const { Column, HeaderCell, Cell, Pagination } = Table;

const NameCell = ({ rowData, dataKey, ...props }) => {
  return (
    <Cell {...props}>
      {dataKey === "discount" ? (
        parseFloat(rowData[dataKey].toString())
      ) : (
        <a>{rowData[dataKey].toLocaleString()}</a>
      )}
    </Cell>
  );
};

const StatusCell = ({ rowData, dataKey, ...props }) => {
  let label, tageColor;
  if (dataKey === "is_active") {
    label = rowData[dataKey] ? "Live" : "Not Active";
    tageColor = rowData[dataKey] ? "green" : "red";
  } else {
    if (rowData[dataKey] === "P") {
      label = "Pending";
      tageColor = "red";
    }
    if (rowData[dataKey] === "A") {
      label = "Approved";
      tageColor = "green";
    }
    if (rowData[dataKey] === "D") {
      label = "Denied";
      tageColor = "yellow";
    }
  }

  return (
    <Cell {...props}>
      <TagGroup>
        <Tag color={tageColor}>{label}</Tag>
      </TagGroup>
    </Cell>
  );
};

const ImageCell = ({ rowData, dataKey, ...props }) => (
  <Cell {...props} style={{ padding: 0 }}>
    <div
      style={{
        width: 40,
        height: 40,
        background: "#f5f5f5",
        borderRadius: 5,
        marginTop: 2,
        overflow: "hidden",
        display: "inline-block"
      }}
    >
      <img src={`${MEDIA_URL}${rowData[dataKey]}`} width="44" />
    </div>
  </Cell>
);

const Menu = ({ onSelect }) => (
  <Dropdown.Menu onSelect={onSelect}>
    <Dropdown.Item eventKey={3}>View</Dropdown.Item>
  </Dropdown.Menu>
);

const MenuPopover = ({ onSelect, ...rest }) => (
  <Popover {...rest} full>
    <Menu onSelect={onSelect} />
  </Popover>
);

let tableBody;

class CustomWhisper extends React.Component {
  constructor(props) {
    super(props);
    this.handleSelectMenu = this.handleSelectMenu.bind(this);
  }
  handleSelectMenu(eventKey, event) {
    console.log(eventKey);
    if (eventKey === 3) {
      this.props.onViewClick(this.props.data);
    }
  }
  render() {
    return (
      <Whisper
        placement="autoVerticalStart"
        trigger="click"
        triggerRef={ref => {
          this.trigger = ref;
        }}
        container={() => {
          return tableBody;
        }}
        speaker={<MenuPopover onSelect={this.handleSelectMenu} />}
      >
        {this.props.children}
      </Whisper>
    );
  }
}

const ActionCell = ({
  rowData,
  dataKey,
  ban,
  edit,
  updateOffer,
  onViewClick,
  ...props
}) => {
  return (
    <Cell {...props} className="link-group">
      <Whisper
        placement="bottom"
        trigger="hover"
        speaker={
          <Tooltip>
            {rowData["is_active"] ? "Make Inactive" : "Make Active"}
          </Tooltip>
        }
      >
        <IconButton
          appearance="subtle"
          icon={<Icon icon={rowData["is_active"] ? "ban" : "check"} />}
          onClick={() => {
            const newValues = {
              offer_id: rowData["id"],
              update_type: 2,
              is_active: !rowData["is_active"]
            };
            updateOffer(newValues);
          }}
        />
      </Whisper>
      {rowData["approval_status"] == "P" ? <Divider vertical /> : null}

      {rowData["approval_status"] == "P" ? (
        <Whisper
          placement="bottom"
          trigger="hover"
          speaker={<Tooltip>Approve</Tooltip>}
        >
          <IconButton
            appearance="subtle"
            icon={<Icon icon="check-square-o" />}
            onClick={() => {
              const newValues = {
                offer_id: rowData["id"],
                update_type: 1,
                approval_status: "A"
              };
              updateOffer(newValues);
            }}
          />
        </Whisper>
      ) : null}

      {rowData["approval_status"] == "P" ? <Divider vertical /> : null}

      {rowData["approval_status"] == "P" ? (
        <Whisper
          placement="bottom"
          trigger="hover"
          speaker={<Tooltip>Deny</Tooltip>}
        >
          <IconButton
            appearance="subtle"
            icon={<Icon icon="close-circle" />}
            onClick={() => {
              const newValues = {
                offer_id: rowData["id"],
                update_type: 1,
                approval_status: "D"
              };
              updateOffer(newValues);
            }}
          />
        </Whisper>
      ) : null}

      <Divider vertical />

      <CustomWhisper
        onViewClick={onViewClick}
        dataId={rowData[dataKey]}
        data={rowData}
      >
        <IconButton appearance="subtle" icon={<Icon icon="more" />} />
      </CustomWhisper>
    </Cell>
  );
};

class CustomColumnTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      checkedKeys: [],
      displayLength: 10,
      loading: false,
      page: 1
    };
  }

  handleChangePage = dataKey => {
    this.props.onPageChange(dataKey);
  };

  render() {
    const { page } = this.state;
    const { data, maxPage, onViewClick, ban, edit, updateOffer } = this.props;

    return (
      <div>
        <Pagination
          lengthMenu={[
            {
              value: 10,
              label: 10
            }
          ]}
          activePage={page}
          displayLength={10}
          total={maxPage}
          onChangePage={this.handleChangePage}
          onChangeLength={this.handleChangeLength}
        />
        <Table
          height={600}
          data={data}
          id="table"
          bodyRef={ref => {
            tableBody = ref;
          }}
        >
          <Column width={150} align="center">
            <HeaderCell>Offer Poster</HeaderCell>
            <ImageCell dataKey="banner" />
          </Column>

          <Column width={150}>
            <HeaderCell>Vendor Shop</HeaderCell>
            <NameCell dataKey="vendor_shop_name" />
          </Column>
          <Column width={250}>
            <HeaderCell>Offer Title</HeaderCell>
            <NameCell dataKey="title" />
          </Column>

          <Column width={120}>
            <HeaderCell>Expiry on</HeaderCell>
            <NameCell dataKey="to_date" />
          </Column>

          <Column width={100}>
            <HeaderCell>Discount %</HeaderCell>
            <NameCell dataKey="discount" />
          </Column>
          <Column width={100}>
            <HeaderCell>Live Status</HeaderCell>
            <StatusCell dataKey="is_active" />
          </Column>
          <Column width={120}>
            <HeaderCell>Approval Status</HeaderCell>
            <StatusCell dataKey="approval_status" />
          </Column>
          {/* <Column width={260}>
            <HeaderCell>Region</HeaderCell>
            <RegionCell dataKey="region" />
          </Column> */}
          {localStorage.getItem("rl") == "1" ? null : (
            <Column width={250}>
              <HeaderCell>Action</HeaderCell>
              <ActionCell
                dataKey="id"
                ban={ban}
                edit={edit}
                updateOffer={updateOffer}
                onViewClick={onViewClick}
              />
            </Column>
          )}
        </Table>
      </div>
    );
  }
}

export default CustomColumnTable;
