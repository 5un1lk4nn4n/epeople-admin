import React, { Component } from "react";
import {
  Icon,
  Content,
  Container,
  FlexboxGrid,
  Form,
  FormControl,
  FormGroup,
  ButtonToolbar,
  ControlLabel,
  Button,
  IconButton,
  ButtonGroup,
  Divider,
  Message,
  Alert,
  Drawer,
  HelpBlock,
  SelectPicker
} from "rsuite";
// constants
import Table from "../components/EmployeeTable";
import Loader from "../components/Loader";
import ApiService from "../services/api";
import { ROLES } from "../constants";

const roleOptions = [
  {
    label: "Sales",
    value: 4,
    role: "sales"
  },
  {
    label: "Admin",
    value: 3,
    role: "admin"
  }
];

class Employees extends Component {
  api = new ApiService();
  page = 1;
  search = null;
  constructor(props) {
    super(props);
    this.state = {
      show: false,
      showDrawer: false,
      loading: false,
      selectedFilter: "all",
      pageLoader: true
    };
    this.close = this.close.bind(this);
    this.open = this.open.bind(this);
    this.closeDrawer = this.closeDrawer.bind(this);
    this.toggleDrawer = this.toggleDrawer.bind(this);
  }

  componentDidMount() {
    this.getEmployees("role=all");
    this.role = this.api.getRole();
  }

  getEmployees = filter => {
    this.api
      .getEmployee(filter)
      .then(res => {
        this.setState({
          pageLoader: false,
          tableLoader: false
        });
        if (res.code === 401) {
          Alert.error(res.message, 5000);
        }
        if (res.code === 400) {
          Alert.error(res.message, 5000);
        }
        if (res.code === 200) {
          this.maxPage = res.max_pages;
          this.setState({
            empData: res.data
          });
        }
      })
      .catch(error => {
        this.setState({
          loading: false
        });
      });
  };
  toggleActive = (userId, isActive) => {
    this.api
      .toggleEmployeActive(userId, isActive)
      .then(res => {
        if (res.code === 401) {
          Alert.error(res.message, 5000);
        }
        if (res.code === 400) {
          Alert.error(res.message, 5000);
        }
        if (res.code === 200) {
          Alert.success(res.message, 5000);
          this.getEmployees("role=all");
        }
      })
      .catch(error => {
        this.setState({
          loading: false
        });
      });
  };
  close() {
    this.setState({
      show: false
    });
  }
  open(size) {
    this.setState({
      size,
      show: true
    });
  }

  closeDrawer() {
    this.setState({
      showDrawer: false
    });
  }
  toggleDrawer(placement) {
    this.setState({
      placement,
      showDrawer: true
    });
  }

  validate = () => {
    if (!this.mobile || !this.name || !this.role) {
      Alert.error("All fields are required", 5000);
      return false;
    }

    return true;
  };

  createCredentials = () => {
    if (this.validate()) {
      this.setState({
        loading: true
      });
      this.createNewCredentials();
    }
  };

  createNewCredentials = () => {
    this.api
      .newCredential(this.name, this.mobile, this.role)
      .then(res => {
        this.setState({
          loading: false
        });
        if (res.code === 401) {
          Alert.error(res.message, 5000);
        }
        if (res.code === 400) {
          Alert.error(res.message, 5000);
        }
        if (res.code === 200) {
          Alert.success("New Cretentials created", 5000);
          this.getEmployees("role=all");
        }
      })
      .catch(error => {
        this.setState({
          loading: false
        });
        console.log("error");
      });
  };
  setFilter = selectedFilter => {
    this.search = selectedFilter;
    this.setState({
      selectedFilter,
      tableLoader: true
    });

    this.getEmployees(`role=${selectedFilter}`);
  };

  getFilter = () => {
    let filter = "";
    if (this.page) {
      filter = `page=${this.page}`;
    }
    if (this.search) {
      filter = `${filter}&role=${this.search}`;
    }
    return filter;
  };

  onPageChange = page => {
    this.page = page;

    this.getEmployees(this.getFilter());
  };
  render() {
    const {
      loading,
      selectedFilter,
      pageLoader,
      empData,
      tableLoader
    } = this.state;
    if (pageLoader) {
      return <Loader />;
    }
    return (
      <Content>
        <div
          style={{
            marginTop: "2%"
          }}
        >
          <Container>
            <FlexboxGrid justify="center">
              <FlexboxGrid.Item colspan={6}>
                <ButtonGroup size="sm">
                  <Button
                    onClick={() => {
                      this.setFilter("all");
                    }}
                    appearance={
                      selectedFilter === "all" ? "primary" : "default"
                    }
                  >
                    All
                  </Button>
                  <Button
                    onClick={() => {
                      this.setFilter(4);
                    }}
                    appearance={selectedFilter === 4 ? "primary" : "default"}
                  >
                    Sales
                  </Button>
                  <Button
                    appearance={selectedFilter === 3 ? "primary" : "default"}
                    onClick={() => {
                      this.setFilter(3);
                    }}
                  >
                    Admin
                  </Button>

                  <Button
                    appearance={selectedFilter === 5 ? "primary" : "default"}
                    onClick={() => {
                      this.setFilter(5);
                    }}
                  >
                    Super Admin
                  </Button>
                </ButtonGroup>
              </FlexboxGrid.Item>
              {this.role === ROLES.SUPER_ADMIN ? (
                <FlexboxGrid.Item colspan={6}>
                  <ButtonToolbar>
                    <IconButton
                      icon={<Icon icon={"plus"} />}
                      size="sm"
                      appearance="primary"
                      onClick={() => this.toggleDrawer("right")}
                    >
                      New User
                    </IconButton>
                  </ButtonToolbar>
                </FlexboxGrid.Item>
              ) : null}
            </FlexboxGrid>
          </Container>
        </div>

        <Table
          loading={tableLoader}
          data={empData}
          toggleActive={this.toggleActive}
          maxPage={this.maxPage}
          onPageChange={this.onPageChange}
          activePage={this.page}
          role={this.role}
        />

        <Drawer
          placement={this.state.placement}
          show={this.state.showDrawer}
          onHide={this.closeDrawer}
        >
          <Drawer.Header>
            <Drawer.Title>New Employee</Drawer.Title>
          </Drawer.Header>
          <Drawer.Body>
            <Container>
              <FlexboxGrid
                style={{
                  flexDirection: "column"
                }}
              >
                <Form>
                  <FormGroup>
                    <ControlLabel>Name</ControlLabel>
                    <FormControl
                      name="name"
                      componentClass="input"
                      onChange={val => {
                        this.name = val;
                      }}
                    />
                    <HelpBlock tooltip>Required.</HelpBlock>
                  </FormGroup>
                  <FormGroup>
                    <ControlLabel>Mobile</ControlLabel>
                    <FormControl
                      name="mobile"
                      componentClass="input"
                      onChange={val => {
                        this.mobile = val;
                      }}
                    />
                    <HelpBlock tooltip>
                      Required.This Mobile number will be used as login username
                    </HelpBlock>
                  </FormGroup>

                  <FormGroup>
                    <ControlLabel>Role</ControlLabel>
                    <SelectPicker
                      onChange={val => {
                        this.role = val;
                      }}
                      data={roleOptions}
                      style={{ width: 224 }}
                    />
                    <HelpBlock tooltip>Required.</HelpBlock>
                  </FormGroup>
                  <FormGroup>
                    <ButtonToolbar>
                      <Button
                        appearance="primary"
                        loading={loading}
                        onClick={() => this.createCredentials()}
                      >
                        <Icon icon="check-circle" /> Create and Send Credentials
                      </Button>
                      <Button
                        appearance="subtle"
                        onClick={this.closeDrawer}
                        disabled={loading}
                      >
                        Cancel
                      </Button>
                    </ButtonToolbar>
                  </FormGroup>
                </Form>
                <Divider />
                <Message description="This will give access to epeoplemart admin portal. Credentials will be sms to this mobile number." />
              </FlexboxGrid>
            </Container>
          </Drawer.Body>
        </Drawer>
      </Content>
    );
  }
}

export default Employees;
