import React from "react";
import { Icon, Container } from "rsuite";
// constants

class Loader extends React.Component {
  render() {
    return (
      <Container
        style={{
          display: "flex",
          flex: 1,
          height: window.innerHeight - 100,
          justifyContent: "center",
          alignItems: "center"
        }}
      >
        <Icon icon="cog" spin size="3x" />
      </Container>
    );
  }
}

export default Loader;
