import React, { Component } from "react";
import {
  Icon,
  Container,
  Panel,
  FlexboxGrid,
  IconButton,
  ButtonGroup,
  Alert,
  Input,
  InputGroup
} from "rsuite";

import ApiService from "../services/api";
// constants
const ENTER_KEY = 13;

const styles = {
  width: 300,
  marginBottom: 20
};

class Login extends Component {
  api = new ApiService();
  constructor() {
    super();
    this.state = {
      loading: false
    };
  }

  componentDidMount(){
    document.addEventListener("keydown", this.handleKeyDown);
  }

  componentWillUnmount() {
    document.removeEventListener("keydown", this.handleKeyDown);
}

  handleKeyDown =(e)=>{
    if(e.keyCode  === ENTER_KEY){
      this.tryLogin()
    }

  }
  tryLogin = () => {
    // Login
    const { loading } = this.state;
    if (loading) {
      return;
    }
    if (this.validate()) {
      this.setState({
        loading: true
      });

      this.getLogin();
    }
  };

  getLogin = () => {
    this.api
      .login(this.mobile, this.password)
      .then(res => {
        this.setState({
          loading: false
        });
        if (res.code === 401) {
          Alert.error(res.message, 5000);
        }
        if (res.code === 400) {
          Alert.error(res.message, 5000);
        }
        if (res.code === 200) {
          this.props.history.push("/vendors");
        }
      })
      .catch(error => {
        this.setState({
          loading: false
        });
        Alert.error("Something went wrong", 5000);
      });
  };

  validate = () => {
    if (!this.mobile || !this.password) {
      Alert.error("Both Mobile and password is required", 5000);
      return false;
    }

    return true;
  };
  render() {
    const { loading } = this.state;
    return (
      <Container
        style={{
          width: "100%",
          height: window.innerHeight
        }}
        className="login-wrapper"
      >
        <Container
          style={{
            width: "100%",
            height: "50%",
            alignItems: "center",
            justifyContent: "center"
          }}
          className="login-container"
        >
          <Panel
            header={<h3>Login to Epeoplemart</h3>}
            style={{
              backgroundColor: "white",

              alignSelf: "center"
            }}
          >
            <FlexboxGrid.Item>
              <div>
                <InputGroup style={styles}>
                  <InputGroup.Addon>
                    <Icon icon="mobile" />
                  </InputGroup.Addon>
                  <Input
                    onChange={val => {
                      this.mobile = val;
                    }}
                  />
                </InputGroup>
                <InputGroup style={styles}>
                  <InputGroup.Addon>
                    <Icon icon="key" />
                  </InputGroup.Addon>
                  <Input
                    type="password"
                    onChange={val => {
                      this.password = val;
                    }}
                  />
                </InputGroup>
              </div>

              <Container
                style={{
                  marginTop: "4%"
                }}
              >
                <ButtonGroup>
                  <IconButton
                    appearance="primary"
                    onClick={this.tryLogin}
                    icon={loading ? null : <Icon icon="angle-double-right" />}
                    placement="right"
                    loading={loading}
                    className="login-btn"
                  >
                    Get In
                  </IconButton>
                </ButtonGroup>
              </Container>
            </FlexboxGrid.Item>
          </Panel>
        </Container>
      </Container>
    );
  }
}

export default Login;
