import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import "rsuite/lib/styles/index.less";

import Protected from "./utils/Protected";
import Login from "./pages/Login";
import Settings from "./pages/Settings";
import Employees from "./pages/Employees";
import Offers from "./pages/Offers";
import Vendors from "./pages/Vendors";
import PageNotFound from "./pages/PageNotFound";

const Main = () => (
  <Router>
    <Switch>
      <Route path="/login" exact component={Login} />
      <Protected path="/settings" exact component={Settings} />
      <Protected path="/epmart-team" exact component={Employees} />
      <Protected path="/offers" exact component={Offers} />
      <Protected path="/vendors" exact component={Vendors} />
      <Route component={PageNotFound} />
    </Switch>
  </Router>
);

ReactDOM.render(<Main />, document.getElementById("root"));
