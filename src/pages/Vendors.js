import React, { Component } from "react";
import {
  Icon,
  Alert,
  Content,
  Container,
  FlexboxGrid,
  ButtonToolbar,
  IconButton,
  Drawer,
  Input,
  InputGroup
} from "rsuite";
import Loader from "../components/Loader";
import ApiService from "../services/api";

import VendorRegistration from "../components/VendorRegistration";
// constants
import Table from "../components/VendorTable";
import VendorInformation from "../components/VendorInformation";

class Vendors extends Component {
  api = new ApiService();
  page = 1;
  role = null;
  search = null;
  constructor(props) {
    super(props);
    this.state = {
      show: false,
      showDrawer: false,
      pageLoader: true,
      drawerLoader: false,
      newVendor: {},
      imgUploadData: {}
    };
    this.close = this.close.bind(this);
    this.open = this.open.bind(this);
    this.closeDrawer = this.closeDrawer.bind(this);
    this.toggleDrawer = this.toggleDrawer.bind(this);
  }

  componentDidMount() {
    this.getVendor("page=1");
    this.role = this.api.getRole();
  }

  getVendor = filter => {
    this.api
      .getVendor(filter)
      .then(res => {
        this.setState({
          pageLoader: false,
          tableLoader: false
        });
        if (res.code === 401) {
          Alert.error(res.message, 5000);
        }
        if (res.code === 400) {
          Alert.error(res.message, 5000);
        }
        if (res.code === 200) {
          this.maxPage = res.max_pages;
          this.setState({
            vendorList: res.data
          });
        }
      })
      .catch(error => {
        this.setState({
          loading: false
        });
        console.log(error);
      });
  };
  close() {
    this.setState({
      show: false
    });
  }
  open(size) {
    this.setState({
      size,
      show: true
    });
  }

  closeDrawer() {
    this.isVendorEdit = false;

    this.setState(
      {
        showDrawer: false
      },
      () => {
        setTimeout(() => {
          this.setState({
            drawerLoader: false,
            vendorData: null
          });
        }, 1500);
      }
    );
  }
  toggleDrawer(placement) {
    this.setState({
      placement,
      showDrawer: true
    });
    this.listCategoriesHandler();
  }

  onViewClick = vendorId => {
    this.vendorId = vendorId;
    this.setState(
      {
        drawerLoader: true
      },
      () => {
        this.getVendorDetails();
      }
    );
    this.toggleDrawer("right");
  };

  getVendorDetails = () => {
    this.api
      .getVendorDetail(this.vendorId)
      .then(res => {
        if (res.code === 401) {
          Alert.error(res.message, 5000);
        }
        if (res.code === 400) {
          Alert.error(res.message, 5000);
        }
        if (res.code === 200) {
          this.setState({
            vendorData: res.data,
            drawerLoader: false
          });
        }
      })
      .catch(error => {
        this.setState({
          loading: false
        });
      });
  };

  onSearch = filter => {
    this.search = filter;
    this.page = 1;
    this.setState({
      tableLoader: true
    });
    this.getVendor(this.getFilter());
  };

  getFilter = () => {
    let filter = "";
    if (this.page) {
      filter = `page=${this.page}`;
    }
    if (this.search) {
      filter = `${filter}&search=${this.search}`;
    }
    return filter;
  };

  onPageChange = page => {
    this.page = page;

    this.getVendor(this.getFilter());
  };

  newVendorCreation = (values, callback) => {
    this.api.createVendor(values).then(res => {
      if (res.code === 400 || res.code === 401) {
        Alert.error(res.message, 5000);
        if (res.error.constructor == Object) {
          Object.keys(res.error).forEach((item, index) => {
            Alert.error(`${item.toUpperCase()}:${res.error[item]}`, 5000);
          });
        } else if (Array.isArray(res.error)) {
          res.error.forEach((item, index) => {
            Alert.error(item, 5000);
          });
        } else {
          Alert.error(res.error);
        }
      } else if (res.code === 200) {
        Alert.success(res.message, 5000);
        this.setState({ newVendor: res.data });
        callback();
      }
    });
  };

  updateVendor = (newVendorValues, callback) => {
    const { imgUploadData } = this.state;
    if (imgUploadData.id) {
      newVendorValues["new_values"]["agreement"] = this.state.imgUploadData.id;
    }
    this.api.updateVendor(newVendorValues).then(res => {
      this.getVendor(this.getFilter());

      if (res.code === 400) {
        Alert.error(res.message, 5000);
      } else if (res.code === 200 || res.code === 201) {
        Alert.success(res.message, 5000);
      }
      callback(res.code);
    });
  };

  editVendor = vendorId => {
    this.setState({
      imgUploadData: {}
    });
    this.vendorId = vendorId;
    this.isVendorEdit = true;
    this.getVendorDetails();
    this.toggleDrawer("right");
  };
  // image upload
  onPictureUplod = picture => {
    if (picture.length === 0) {
      Alert.error("Invalid image format", 5000);
      return;
    }
    const fd = new FormData();
    fd.append("file", picture[0], picture[0].name);
    fd.append("file_type", "A");
    this.api.uploadImg(fd).then(res => {
      if (res.code === 400) {
        Alert.error(res.message, 5000);
      } else if (res.code === 200 || res.code === 201) {
        Alert.success(res.message, 5000);
        this.setState({ imgUploadData: res.data });
      }
    });
  };

  // list categories
  listCategoriesHandler = () => {
    let listCategoriesReq = {
      apiURL: "categories"
    };
    this.api.getAPIRequest(listCategoriesReq).then(res => {
      let catTemp = [];
      if (res.data.length > 0) {
        catTemp = res.data.slice(0, 20);
      }
      catTemp.forEach(el => {
        return (
          (el.label = el.name),
          (el.value = el.id),
          (el.id = el.id),
          (el.icon = el.icon)
        );
      });
      this.setState({
        listCategories: catTemp
      });
    });
  };

  capitalize = s => {
    if (typeof s !== "string") return "";
    return s.charAt(0).toUpperCase() + s.slice(1);
  };

  searchCategories = search => {
    this.api.getCategories(this.capitalize(search)).then(res => {
      res.data.forEach(el => {
        return (
          (el.label = el.name),
          (el.value = el.id),
          (el.id = el.id),
          (el.icon = el.icon)
        );
      });
      this.setState({
        listCategories: res.data
      });
    });
  };

  // vendor submission
  vendorSubmission = callback => {
    const { newVendor, imgUploadData } = this.state;
    let vendorSubmission = {
      apiURL: "register-vendor",
      request: {
        step: 2,
        vendor_id: newVendor.vendor_id,
        attachment_id: imgUploadData.id
      }
    };
    this.api
      .postAPIRequest(vendorSubmission)
      .then(res => {
        if (res.code === 401) {
          Alert.error(res.message, 5000);
        }
        if (res.code === 400) {
          Alert.error(res.message, 5000);
        }
        if (res.code === 200) {
          Alert.success("Vendor Added and successfully sent credentials", 5000);
          this.getVendor();
        }
        callback(res.code);
      })
      .catch(error => {
        callback();
      });
  };

  renderDrawer = () => {
    const {
      vendorData,
      drawerLoader,
      newVendor,
      pageLoader,
      listCategories
    } = this.state;

    if (drawerLoader) {
      return <Loader />;
    }

    if (this.isVendorEdit && !vendorData) {
      return <Loader />;
    }
    if (vendorData && !this.isVendorEdit) {
      return <VendorInformation data={vendorData} />;
    }

    return (
      <VendorRegistration
        listCategories={listCategories}
        vendorSubmission={this.vendorSubmission}
        onPictureUplod={this.onPictureUplod}
        newVendorCreation={this.newVendorCreation}
        newVendor={newVendor}
        onCancel={this.closeDrawer}
        isVendorEdit={this.isVendorEdit}
        vendorInfo={vendorData}
        editVendorInfo={this.updateVendor}
        role={this.role}
        searchCat={this.searchCategories}
      />
    );
  };

  render() {
    const {
      loading,
      selectedFilter,
      pageLoader,
      vendorList,
      tableLoader,
      drawerLoader,
      vendorData
    } = this.state;
    if (pageLoader) {
      return <Loader />;
    }
    return (
      <Content>
        {this.role != 1 ? (
          <div
            style={{
              marginTop: "2%"
            }}
          >
            <Container>
              <FlexboxGrid justify="center">
                <FlexboxGrid.Item
                  colspan={4}
                  style={{
                    marginRight: "5%"
                  }}
                >
                  <InputGroup size="sm">
                    <Input
                      placeholder="Search by Name"
                      onChange={this.onSearch}
                    />
                    <InputGroup.Addon>
                      <Icon icon="search" />
                    </InputGroup.Addon>
                  </InputGroup>
                </FlexboxGrid.Item>

                <FlexboxGrid.Item colspan={4}>
                  <ButtonToolbar>
                    <IconButton
                      icon={<Icon icon={"plus"} />}
                      size="sm"
                      appearance="primary"
                      onClick={() => this.toggleDrawer("right")}
                    >
                      Register Vendor
                    </IconButton>
                  </ButtonToolbar>
                </FlexboxGrid.Item>
              </FlexboxGrid>
            </Container>
          </div>
        ) : null}

        <Table
          loading={tableLoader}
          data={vendorList}
          maxPage={this.maxPage}
          onPageChange={this.onPageChange}
          onViewClick={this.onViewClick}
          activePage={this.page}
          role={this.role}
          updateVendor={this.updateVendor}
          editVendor={this.editVendor}
        />
        <Drawer
          placement={this.state.placement}
          show={this.state.showDrawer}
          onHide={this.closeDrawer}
          size="md"
          className="custom-drawyer"
          backdrop="static"
        >
          <Drawer.Header>
            <Drawer.Title>
              {drawerLoader || vendorData
                ? "Vendor Information"
                : " New Vendor Registration"}
            </Drawer.Title>
          </Drawer.Header>
          <Drawer.Body>{this.renderDrawer()}</Drawer.Body>
        </Drawer>
      </Content>
    );
  }
}

export default Vendors;
