import * as React from "react";
import { Link } from "react-router-dom";
const notFound = require("../assets/images/404.png");

const ErrorPage = ({ code = 404 }) => (
  <div
    className="error-page"
    style={{
      display: "flex",
      flex: 1,
      flexDirection: "column",
      justifyContent: "center",
      alignItems: "center",
      height: window.innerHeight
    }}
  >
    <div className="item">
      <img
        src={notFound}
        style={{
          width: 200
        }}
        alt="Not Found"
      />
    </div>
    <div className="text">
      <h1 className="code">{code}</h1>
      <p>Page Not Found</p>
      <p>
        Back to <Link to="/vendors">Home</Link>
      </p>
    </div>
  </div>
);

export default ErrorPage;
