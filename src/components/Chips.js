import React from "react";
import { Input, IconButton, Icon, TagGroup, Tag } from "rsuite";

class DynamicTag extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      typing: false,
      inputValue: "",
      tags: this.props.hashTags
    };
    this.handleButtonClick = this.handleButtonClick.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleInputConfirm = this.handleInputConfirm.bind(this);
  }
  handleButtonClick() {
    this.setState(
      {
        typing: true
      },
      () => {
        this.input.focus();
      }
    );
  }
  handleInputChange(inputValue) {
    this.setState({ inputValue });
  }
  handleInputConfirm() {
    const { inputValue, tags } = this.state;
    const nextTags = inputValue ? [...tags, inputValue] : tags;
    this.setState({
      tags: nextTags,
      typing: false,
      inputValue: ""
    });
    var term = "";
    nextTags.forEach(el => {
      term += "#" + el;
    });
    this.props.renderSearchTerm(term);
  }
  handleTagRemove(tag) {
    const { tags } = this.state;
    const nextTags = tags.filter(item => item !== tag);
    this.setState({
      tags: nextTags
    });
    var term = "";
    nextTags.forEach(el => {
      term += "#" + el;
    });
    this.props.renderSearchTerm(term);
  }
  renderInput() {
    const { typing, inputValue } = this.state;

    if (typing) {
      return (
        <Input
          className="tag-input"
          inputRef={ref => {
            this.input = ref;
          }}
          size="xs"
          style={{ width: 70 }}
          value={inputValue}
          onChange={this.handleInputChange}
          onBlur={this.handleInputConfirm}
          onPressEnter={this.handleInputConfirm}
        />
      );
    }

    return (
      <IconButton
        className="tag-add-btn"
        onClick={this.handleButtonClick}
        icon={<Icon icon="plus" />}
        appearance="ghost"
        size="xs"
      />
    );
  }
  render() {
    const { tags } = this.state;

    return (
      <TagGroup style={{ margin: "0" }}>
        {tags.map((item, index) => (
          <Tag
            key={index}
            closable
            style={{ marginLeft: "0", marginTop: "0", marginRight: "10px" }}
            onClose={() => {
              this.handleTagRemove(item);
            }}
          >
            {item}
          </Tag>
        ))}
        {this.renderInput()}
      </TagGroup>
    );
  }
}

export default DynamicTag;
