/* 
Role numbers for user list
VENDOR = 1
USER = 2
ADMIN = 3
SALES = 4
SUPER_ADMIN = 5
*/

const HEADER_MENU = {
  1: [
    {
      eventKey: 1,
      icon: "squares",
      name: "My Shops",
      url: "/vendors"
    },
    {
      eventKey: 2,
      icon: "gift",
      name: "Offers",
      url: "/offers"
    }
  ],
  3: [
    {
      eventKey: 1,
      icon: "gift",
      name: "Offers",
      url: "/offers"
    },
    {
      eventKey: 2,
      icon: "street-view",
      name: "Vendors",
      url: "/vendors"
    }
  ],
  4: [
    {
      eventKey: 2,
      icon: "street-view",
      name: "Vendors",
      url: "/vendors"
    }
  ],
  5: [
    {
      eventKey: 1,
      icon: "gift",
      name: "Offers",
      url: "/offers"
    },
    {
      eventKey: 2,
      icon: "street-view",
      name: "Vendors",
      url: "/vendors"
    },
    {
      eventKey: 3,
      icon: "group",
      name: "Epmart Team",
      url: "/epmart-team"
    }
  ]
};

export default HEADER_MENU;
